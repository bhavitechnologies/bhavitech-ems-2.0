export interface Transaction {
  id: string;
  amount: number;
  currency: string;
  order_id: string;
  provider_id: string;
  settlement_id: string;
  type: string;
  settlement: TransactionSettlement;
  created_at: Date;
  updated_at: Date;
}

export interface TransactionSettlement {
  id: string;
  metadata: TransactionSettlementMetadata;
  provider: string;
  created_at: Date;
  updated_at: Date;
}

export interface TransactionSettlementMetadata {
  brand: string;
  holder: string;
  lastFourDigits: string;
  expirationMonth: number;
  expirationYear: number;
}
