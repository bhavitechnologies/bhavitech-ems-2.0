export interface Session {
  token_type?: string;
  access_token?: string;
  refresh_token?: string;
  expires_in?: number;
}

export interface Login {
  Email?: string;
  Password?: string;

}
