export interface Holidaycalender {
    Id: number;
    Year: string;
    HolidayID: number;
    Frequency: string;
    FromDate: string;
    ToDate: string;
    HolidayName: string;
}
