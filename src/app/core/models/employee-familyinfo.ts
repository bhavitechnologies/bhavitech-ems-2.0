export interface EmployeeFamilyinfo {
  DepID: number;
  Name: string;
  Relationship: string;
  DOB: Date;
  Occupation: number;
  EmployeeName: string;
}
