export interface EmployeeFormList {
  Id: number;
  EmployeeCode: string;
  FirstName: string;
   Email: string;
   ContactNo: string;
   DepartmentName: string;
}
