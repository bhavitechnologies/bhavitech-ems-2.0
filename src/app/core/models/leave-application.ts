export interface Leaveapplication {
  Id: number;
  EmpId: number;
  Name: string;
  Leavetype: string;
  FromDate: Date;
  ToDate: Date;
  Reasonforleave: string;
  Status: string;
  Comment: string ;

}
