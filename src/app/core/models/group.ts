export interface Group {
  group_id: string;
  name: string;
  total_members: number;
}

export interface GroupCollection {
  current_page: number;
  from: number;
  items: Group[];
  per_page: number;
  to: number;
  total: number;
}

export interface QueryGroup {
  [param: string]: string;
}
