export interface EmployeeEducation {
  EduID: number;
  EmpId: number;
  SchoolUniversity: string;
  Degree: string;
  Grade: string;
  PassingOfYear: number;
  EmployeeName: string;
}
