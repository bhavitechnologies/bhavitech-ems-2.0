import { Address } from './address';

export interface Account {
  id: string;
  name: string;
  address: Address;
}

export interface AccountStore {
  account_store_id: string;
  account_id: string;
  currency: string;
  name: string;
  slug: string;
  created_at: Date;
  update_at: Date|null;
}
