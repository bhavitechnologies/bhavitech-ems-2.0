export interface Address {
  id?: string;
  postal_code?: string;
  country?: string;
  county?: string;
  city?: string;
  line1?: string;
  line2?: string;
}
