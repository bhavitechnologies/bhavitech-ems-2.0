export interface Product {
  id: string;
  account_id: string;
  active: boolean;
  name: string;
  price: number;
  quantity: number | null;
  type: string;
  created_at: Date;
  updated_at: Date | null;
}

export interface ProductCollection {
  current_page: number;
  from: number;
  items: Product[];
  per_page: number;
  to: number;
  total: number;
}

export interface ProductQuery {
  name: string|null;
  type: string|null;
}
