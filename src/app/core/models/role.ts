export interface Role {
  Id: number;
  RoleID: number;
  CompanyID: number;
  CompanyName: string;
  Role: string;
}
