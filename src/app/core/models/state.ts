// export class State {
//   CountryID: number;

//   constructor(
//     StateID: number,
//     StateName: string,
//     CountryID: number) { }
// }
export interface State {

    StateID: number;
    StateName: string;
    CountryID: number;
    CountryName: string;
}
