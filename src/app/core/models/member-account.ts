export interface MemberAccount {
  id?: string;
  name?: string;
  email?: string;
  password?: string;
  remember_me?: boolean;
}
