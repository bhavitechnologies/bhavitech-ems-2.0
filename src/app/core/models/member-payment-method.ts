export interface MemberPaymentMethod {
  id: string;
  member_id: string;
  active: boolean;
  default: boolean;
  card?: MemberPaymentMethodCard;
  created_at?: Date;
  updated_at: Date;
}

export interface MemberPaymentMethodCard {
  brand: string;
  card_holder: string;
  expiration_month: string;
  expiration_year: string;
  last_four_digits: string;
}

export interface MemberPaymentMethodCollection {
  current_page: number;
  from: number;
  items: MemberPaymentMethod[];
  per_page: number;
  to: number;
  total: number;
}
