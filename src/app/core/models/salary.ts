export interface Salary {
  Id: number;
  EmpId: number;
  BasicSalary: number;
  Month: string;
  Year: string;
  BankName: string;
  AccountNo: string;
  ReEnterAccountNo: string;
  AccountHolderName: string;
  IFSCcode: string;
  TaxDeduction: string;
  EmployeeCode: string;
  EmployeeName: string;
  HRA: number;
  SA: number;
  MA: number;
  CCA: number;
  LTC: number;
  TA: number;
  MR: number;
  GrossPay: number;
  PT: number;
 // PF  :number;
  TDS: number;
  OtherDed: number;
  Leave: number;
  NetPay: number;
}
