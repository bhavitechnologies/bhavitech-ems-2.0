import { Account } from './account';
import { Product } from './product';

export interface Membership {
  id: string;
  account_id: string;
  member_id: string;
  order_id: string;
  product_id: string;
  active: boolean;
  price: number;
  ends_in: Date | null;
  starts_at: Date;
  created_at: Date;
  updated_at: Date | null;
  account: Account;
  product: Product;
}

export interface MembershipCollection {
  current_page: number;
  from: number;
  items: Membership[];
  per_page: number;
  to: number;
  total: number;
}

export interface MembershipQuery {
  name?: string;
  active?: boolean;
}
