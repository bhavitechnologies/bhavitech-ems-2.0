export interface EmployeeWorkexps {
  WorkID: number;
  CompanyName: string;
  Designation: string;
  FromDate: Date;
  ToDate: Date;
  FirstName: string;
  LastName: string;
}
