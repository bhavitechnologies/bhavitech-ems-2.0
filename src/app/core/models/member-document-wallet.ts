import { Account } from './account';

export interface MemberDocument {
  id: string;
  member_id: string;
  document_id: string;
  label: string;
  created_at: Date;
  updated_at?: Date;
  document: Document;
  shared_with: Account[];
}

export interface Document {
  document_id: string;
  filename: string;
  filepath: string;
}

export interface MemberDocumentCollection {
  current_page: number;
  from: number;
  items: MemberDocument[];
  per_page: number;
  to: number;
  total: number;
}

export interface DocumentQuery {
  label?: string;
}
