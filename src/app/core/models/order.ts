import { Account } from './account';
import { Member } from './member';
import { Transaction } from './transaction';

export interface Order {
  id: string;
  amount: number;
  currency: string;
  paid_in?: Date;
  recurrent: boolean;
  recurring_order_id: string;
  status: string;
  type: string;
  account: Account;
  member: Member;
  transaction: Transaction;
  created_at?: Date;
  updated_at: Date;
}

export interface OrderCollection {
  current_page: number;
  from: number;
  per_page: number;
  to: number;
  total: number;
  items: Order[];
}

export interface QueryOrders {
  created_at: Date;
  account: string;
  type: string;
}
