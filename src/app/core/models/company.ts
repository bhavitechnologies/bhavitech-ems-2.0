export interface Company {
  CompanyID: number;
  CompanyName: string;
  Address: string;
  CountryID: number;
  CountryName: string;
  StateID: number;
  StateName: string;
  CityID: number;
 CityName: string;
  PostalCode: number;
  Website: string;
  Email: string;
  ContactPerson: string;
  ContactNo: string;
  FaxNo: string;
  PanNo: string;
  GSTNo: string;
  CINNo: string;
}
