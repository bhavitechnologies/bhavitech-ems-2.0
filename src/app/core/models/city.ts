// export class City {
//   StateID: number;

//   constructor(
//     CityID: number,
//     CityName: string,
//     StateID: number) { }
// }
export interface City {
    CityID: number;
    CityName: string;
    StateID: number;
    StateName: string
}
