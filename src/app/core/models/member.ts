import { Address } from './address';

export interface MemberCheckForExistingEmail {
  email: string;
  email_exists?: boolean;
}

export interface Member {
  id?: string;
  first_name: string;
  last_name: string;
  email?: string;
  mobile: string;
  address?: Address;
  user?: Address;
  updated_at?: Date;
}
