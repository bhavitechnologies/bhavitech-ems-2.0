export interface EmployeeForm {
  Id: number;
  CompanyID: number;
  Email: string;
  Password: string;
  RoleID: string;
  Title: string;
  FirstName: string;
  MiddleName: string;
  LastName: string;
  DOB: Date;
  ContactNo: string;
  EmployeeCode: string;
  DepartmentID: string;
  PositionID: string;
  DateOfJoining: Date;
  TerminateDate: Date;
  Photo: string;

  Role: string;

  DepartmentName: string;
  PositionName: string;
  CompanyName: string;
  Gender: string;
  Age: number;
  EmergencyContactNo: string;
  PANcardNo: string;
  BloodGroup: string;
  Hobbies: string;
  // PresentAddress: string;
  Address1: string;
  Address2: string;
  Address3: string;
  CountryID: null;
  StateID: null;
  CityID: null;
  Pincode: null;
  IsSameAsPresent: null;
  PermanetAddress: string;
}
