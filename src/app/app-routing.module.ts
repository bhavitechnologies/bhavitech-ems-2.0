import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticationGuard } from './core/guards/authentication.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/authentication/sign-in',
    pathMatch: 'full'
  },
  {
    path: 'member',
    children: [
      {
        path: '',
        loadChildren: () => import('./domain/member/member.module').then(m => m.MemberModule)
      }
    ]
  },
  {
    path: 'employee',
    children: [
      {
        path: '',
        loadChildren: () => import('./domain/employee/employee.module').then(m => m.EmployeeModule)
      }
    ]
  },
  {
    path: 'admin',
    children: [
      {
        path: '',
        loadChildren: () => import('./domain/admin/admin.module').then(m => m.AdminModule)
      }
    ]
  },
  {
    path: 'authentication',
    children: [
      {
        path: '',
        loadChildren: () => import('./domain/authentication/authentication.module').then(m => m.AuthenticationModule)
      }
    ]
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
