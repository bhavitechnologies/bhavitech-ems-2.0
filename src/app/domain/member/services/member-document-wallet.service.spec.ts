import { TestBed } from '@angular/core/testing';

import { MemberDocumentWalletService } from './member-document-wallet.service';

describe('MemberDocumentWalletService', () => {
  let service: MemberDocumentWalletService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MemberDocumentWalletService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
