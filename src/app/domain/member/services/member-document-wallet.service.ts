// import { environment } from './../../../../environments/environment.example';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { DocumentQuery, MemberDocument, MemberDocumentCollection } from '../../../core/models/member-document-wallet';

@Injectable({
  providedIn: 'root'
})
export class MemberDocumentWalletService {
  // api = `${ environment.api }/${ environment.apiVersion }/member/documents`;
  headers = new HttpHeaders({
    Accept: 'application/json',
    'Content-Type': 'application/json'
  });

  constructor(private httpClient: HttpClient) {
  }

  private static handleError(error: HttpErrorResponse): Observable<never> {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${ error.status }, ` +
        `body was: ${ error.error }`);
    }

    return throwError(error);
  }

  // fetch(perPage: number = 9, page: number = 1, query: DocumentQuery): Observable<MemberDocumentCollection> {
  //   const querySerialized: any = query;
  //   const params = new HttpParams({ fromObject: querySerialized })
  //     .set('per_page', perPage.toString())
  //     .set('page', page.toString());

  //   return this.httpClient.get<MemberDocumentCollection>(`${ this.api }`, { headers: this.headers, params })
  //     .pipe(
  //       catchError(MemberDocumentWalletService.handleError)
  //     );
  // }

  // get(documentId: string): Observable<MemberDocument> {
  //   return this.httpClient.get<MemberDocument>(`${ this.api }/${ documentId }`, { headers: this.headers })
  //     .pipe(
  //       catchError(MemberDocumentWalletService.handleError)
  //     );
  // }

  // post(payload: FormData): Observable<any> {
  //   return this.httpClient.post<any>(`${ this.api }`, payload, {
  //     reportProgress: true,
  //     observe: 'events'
  //   })
  //     .pipe(
  //       catchError(MemberDocumentWalletService.handleError)
  //     );
  // }

  // put(documentId: string, accounts: string[]): Observable<any> {
  //   return this.httpClient.put<any>(`${ this.api }/${ documentId }/share`, { accounts }, { headers: this.headers })
  //     .pipe(
  //       catchError(MemberDocumentWalletService.handleError)
  //     );
  // }

  // download(documentId: string): Observable<any> {
  //   return this.httpClient.get(`${ this.api }/${ documentId }/download`, { responseType: 'blob' });
  // }

  // delete(documentId: string): Observable<never> {
  //   return this.httpClient.delete<never>(`${ this.api }/${ documentId }`, { headers: this.headers })
  //     .pipe(
  //       catchError(MemberDocumentWalletService.handleError)
  //     );
  // }
}
