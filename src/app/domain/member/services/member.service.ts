import { EmployeeEducationById } from './../../../core/models/vmEmployeeEducationEduID';
import { EmployeeWorkexps } from './../../../core/models/employee-workexps';
import { EmployeeFamilyinfo } from './../../../core/models/employee-familyinfo';
import { EmployeeEducation } from './../../../core/models/employee-education';
import { Leaveapplication } from './../../../core/models/leave-application';
import { State } from './../../../core/models/state';
import { Country } from './../../../core/models/country';
import { City } from './../../../core/models/city';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
// import { environment } from '../../../../environments/environment.example';
import { rootURL } from '../../../../environments/environment.example';
import { Account } from '../../../core/models/account';
import { GroupCollection, QueryGroup } from '../../../core/models/group';
import { Member, MemberCheckForExistingEmail } from '../../../core/models/member';
import { MemberPaymentMethod, MemberPaymentMethodCollection } from '../../../core/models/member-payment-method';
import { MembershipCollection, MembershipQuery } from '../../../core/models/membership';
import { Order, OrderCollection, QueryOrders } from '../../../core/models/order';
import { Company } from './../../../core/models/company';
import { Department } from './../../../core/models/department';
import { EmployeeForm } from './../../../core/models/employee-form.model';
import { Holidaycalender } from './../../../core/models/holidaycalender.model';
import { Holidaytype } from './../../../core/models/holidaytype.model';
import { Position } from './../../../core/models/position';
import { Role } from './../../../core/models/role';
import { Salary } from './../../../core/models/salary';

@Injectable({
  providedIn: 'root'
})
export class MemberService {
  // api = `${ environment.api }/${ environment.apiVersion }/member`;
  apiNew = `${rootURL}`;
  headers = new HttpHeaders({
    Accept: 'application/json',
    'Content-Type': 'application/json'
  });

  constructor(private httpClient: HttpClient) {
  }

  private static handleError(error: HttpErrorResponse): Observable<never> {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${ error.status }, ` +
        `body was: ${ error.error }`);
    }

    return throwError(error);
  }

  getMyEmployeeList(): Observable<EmployeeForm[]> {
    return this.httpClient.get<EmployeeForm[]>(rootURL + '/EmployeeForm')
      .pipe(
        catchError(MemberService.handleError)
      );
  }

  getList(): Observable<EmployeeForm[]> {
    return this.httpClient.get<EmployeeForm[]>(rootURL + '/EmployeeForm');
  }

  getCompanytList(): Observable<Company[]> {
    return this.httpClient.get<Company[]>(rootURL + '/Company');
  }

  getRoleList(): Observable<Role[]> {
    return this.httpClient.get<Role[]>(rootURL + '/Role');
  }

  getDepartmentList(): Observable<Department[]> {
    return this.httpClient.get<Department[]>(rootURL + '/Department');
  }

  getPositionList(): Observable<Position[]> {
    return this.httpClient.get<Position[]>(rootURL + '/Position');
  }

  // Insert API
  insertEmployee(data: EmployeeForm): Observable<EmployeeForm> {
    return this.httpClient.post<EmployeeForm>(rootURL + '/EmployeeForm', data);
  }

  // Delete API
  deleteEmployee(Id: number) {
    return this.httpClient.delete(rootURL + '/EmployeeForm/' + Id);
  }

  // Update API

  updateEmployee(data: EmployeeForm): Observable<EmployeeForm> {
    return this.httpClient.put<EmployeeForm>(rootURL + '/EmployeeForm/' + data.Id, data);
  }

  // ------- Salary Module ---------------
  getSalaryList(): Observable<Salary[]> {
    return this.httpClient.get<Salary[]>(rootURL + '/Salary').pipe(
      catchError(MemberService.handleError)
    );
  }

  getEmpSalaryDetailsByID(id: number): Observable<EmployeeForm[]> {
    return this.httpClient.get<EmployeeForm[]>(rootURL + '/Salary/' + id).pipe(
      catchError(MemberService.handleError)
    );
  }
  updateEmployeeSalary(data: Salary): Observable<Salary> {
    return this.httpClient.put<Salary>(rootURL + '/Salary/' + data.Id, data);
  }
  getEmployeeName(): Observable<EmployeeForm[]> {
    return this.httpClient.get<EmployeeForm[]>(rootURL + '/EmployeePersonalInfo');
  }

  insertSalaryDetails(data: Salary): Observable<Salary> {
    return this.httpClient.post<Salary>(rootURL + '/Salary', data);
  }

  deleteSalaryDetails(Id: number) {
    return this.httpClient.delete(rootURL + '/Salary/' + Id);
  }

  // ------ Holiday Calander Module ------------

  getHolidaysList(): Observable<Holidaycalender[]>  {
    return this.httpClient.get<Holidaycalender[]>(rootURL + '/HolidayCalender').pipe(
        catchError(MemberService.handleError)
      );
  }
  updateHolidayList(data: Holidaycalender): Observable<Holidaycalender> {
    return this.httpClient.put<Holidaycalender>(rootURL + '/HolidayCalender/' + data.Id, data);
  }
  getHolidayListByID(id: number): Observable<Holidaycalender> {
    return this.httpClient.get<Holidaycalender>(rootURL + '/HolidayCalender/' + id).pipe(
      catchError(MemberService.handleError)
    );
  }

  insertHoliday(data: Holidaycalender): Observable<Holidaycalender> {
    return this.httpClient.post<Holidaycalender>(rootURL + '/HolidayCalender', data);
  }

  deleteHoliday(Id: number) {
    return this.httpClient.delete(rootURL + '/HolidayCalender/' + Id);
  }

  // ------ Holiday Type Module ------------

  getHolidaysTypeList(): Observable<Holidaytype[]>  {
    return this.httpClient.get<Holidaytype[]>(rootURL + '/HolidayType').pipe(
        catchError(MemberService.handleError)
      );
  }

  updateHolidayType(data: Holidaytype): Observable<Holidaytype> {
    return this.httpClient.put<Holidaytype>(rootURL + '/HolidayType/' + data.Id, data);
  }

  getHolidayTypeByID(id: number): Observable<Holidaytype> {
    return this.httpClient.get<Holidaytype>(rootURL + '/HolidayType/' + id).pipe(
      catchError(MemberService.handleError)
    );
  }

  insertHolidayType(data: Holidaytype): Observable<Holidaytype> {
    return this.httpClient.post<Holidaytype>(rootURL + '/HolidayType', data);
  }

  deleteHolidayType(Id: number) {
    return this.httpClient.delete(rootURL + '/HolidayType/' + Id);
  }

  // --------- Company Module --------

  getCityList(): Observable<City[]>  {
    return this.httpClient.get<City[]>(rootURL + '/Cities').pipe(
        catchError(MemberService.handleError)
      );
  }

  getCountryList(): Observable<Country[]>  {
    return this.httpClient.get<Country[]>(rootURL + '/Countries').pipe(
        catchError(MemberService.handleError)
      );
  }

  getStateList(): Observable<State[]>  {
    return this.httpClient.get<State[]>(rootURL + '/States').pipe(
        catchError(MemberService.handleError)
      );
  }

  insertCompany(data: Company): Observable<Company> {
    return this.httpClient.post<Company>(rootURL + '/Company', data);
  }
  getCompanyByID(id: number): Observable<Company> {
    return this.httpClient.get<Company>(rootURL + '/Company/' + id).pipe(
      catchError(MemberService.handleError)
    );
  }
  updateCompany(data: EmployeeForm): Observable<Company> {
    return this.httpClient.put<Company>(rootURL + '/Company/' + data.CompanyID, data);
  }
  deleteCompany(CompanyID: number) {
    return this.httpClient.delete(rootURL + '/Company/' + CompanyID);
  }

  // -------------- Role Module ------------------

  insertRole(data: Role): Observable<Role> {
    return this.httpClient.post<Role>(rootURL + '/Role', data);
  }

  deleteRole(Id: number) {
    return this.httpClient.delete(rootURL + '/Role/' + Id);
  }
  updateRole(data: Role): Observable<Role> {
    return this.httpClient.put<Role>(rootURL + '/Role/' + data.Id, data);
  }

  getRoleByID(id: number): Observable<Role> {
    return this.httpClient.get<Role>(rootURL + '/Role/' + id).pipe(
      catchError(MemberService.handleError)
    );
  }

  // ------------- Position Module ---------------

  insertPosition(data: Position): Observable<Position> {
    return this.httpClient.post<Position>(rootURL + '/Position', data);
  }
  getPositionByID(id: number): Observable<Position> {
    return this.httpClient.get<Position>(rootURL + '/Position/' + id).pipe(
      catchError(MemberService.handleError)
    );
  }

  updatePosition(data: Position): Observable<Position> {
    return this.httpClient.put<Position>(rootURL + '/Position/' + data.PositionID, data);
  }
  deletePosition(Id: number) {
    return this.httpClient.delete(rootURL + '/Position/' + Id);
  }

  // ------------ Department Module ---------------

  insertDepartment(data: Department): Observable<Department> {
    return this.httpClient.post<Department>(rootURL + '/Department', data);
  }
  getDepartmentByID(id: number): Observable<Department> {
    return this.httpClient.get<Department>(rootURL + '/Department/' + id).pipe(
      catchError(MemberService.handleError)
    );
  }

  updateDepartment(data: Department): Observable<Department> {
    return this.httpClient.put<Department>(rootURL + '/Department/' + data.DepartmentID, data);
  }
  deleteDepartment(Id: number) {
    return this.httpClient.delete(rootURL + '/Department/' + Id);
  }

  // ------------ Leave Application Module -----------

  getLeaveApplicationList(): Observable<Leaveapplication[]>  {
    return this.httpClient.get<Leaveapplication[]>(rootURL + '/LeaveApplications').pipe(
        catchError(MemberService.handleError)
      );
  }

  getPerticularLeaveData(id: number): Observable<Leaveapplication> {
    return this.httpClient.get<Leaveapplication>(rootURL + '/GettblLeaveApplicationById/' + id).pipe(
      catchError(MemberService.handleError)
    );
  }

  updatePerticularLeaveData(data: Leaveapplication): Observable<Leaveapplication> {
    return this.httpClient.put<Leaveapplication>(rootURL + '/LeaveApplications/' + data.Id, data);
  }

  deleteLeaveApplication(Id: number) {
    return this.httpClient.delete(rootURL + '/LeaveApplications/' + Id);
  }

  // ------------ Employee Personal Details Module -------------

  getEmpDetailsByID(EmpId: number): Observable<EmployeeForm[]> {
    return this.httpClient.get<EmployeeForm[]>(rootURL + '/EmployeePersonalInfo/' + EmpId).pipe(
      catchError(MemberService.handleError)
    );
  }
  updateEmpDetails(data: EmployeeForm): Observable<EmployeeForm> {
    return this.httpClient.put<EmployeeForm>(rootURL + '/EmployeePersonalInfo', data);
  }
  getEmpEducationById(EmpId: number): Observable<EmployeeEducation[]> {
    return this.httpClient.get<EmployeeEducation[]>(rootURL + '/EmployeeEducation/' + EmpId).pipe(
      catchError(MemberService.handleError)
    );
  }


  getEmpEducationBy(EmpId: number): Observable<EmployeeEducation[]> {
    return this.httpClient.get<EmployeeEducation[]>(rootURL + '/GettblEmployeeWorkExpByEduID/' + EmpId).pipe(
      catchError(MemberService.handleError)
    );
  }

  updateEmpEducation(data: EmployeeEducation): Observable<EmployeeEducation> {
    return this.httpClient.put<EmployeeEducation>(rootURL + '/EmployeeEducation/' + data.EduID, data);
  }

  insertEmpEducation(data: EmployeeEducation): Observable<EmployeeEducation> {
    return this.httpClient.post<EmployeeEducation>(rootURL + '/EmployeeEducation', data);
  }

  deleteEmpEducation(Id: number) {
    return this.httpClient.delete(rootURL + '/EmployeeEducation/' + Id);
  }

  getEmpFamilyInfoById(EduId: number): Observable<EmployeeFamilyinfo[]> {
    return this.httpClient.get<EmployeeFamilyinfo[]>(rootURL + '/EmployeeFamilyInfo/' + EduId).pipe(
      catchError(MemberService.handleError)
    );
  }

  getFamilyInfoBy(DepId: number): Observable<EmployeeFamilyinfo[]> {
    return this.httpClient.get<EmployeeFamilyinfo[]>(rootURL + '/GetEmployeeFamilyInfoByDepID/' + DepId).pipe(
      catchError(MemberService.handleError)
    );
  }

  insertEmpFamilyInfo(data: EmployeeFamilyinfo): Observable<EmployeeFamilyinfo> {
    return this.httpClient.post<EmployeeFamilyinfo>(rootURL + '/EmployeeFamilyInfo', data);
  }
  updateEmpFamily(data: EmployeeFamilyinfo): Observable<EmployeeFamilyinfo> {
    return this.httpClient.put<EmployeeFamilyinfo>(rootURL + '/EmployeeFamilyInfo/' + data.DepID, data);
  }
  deleteFamilyInfo(Id: number) {
    return this.httpClient.delete(rootURL + '/EmployeeFamilyInfo/' + Id);
  }

  getEmpWorkExperienceById(EmpId: number): Observable<EmployeeWorkexps[]> {
    return this.httpClient.get<EmployeeWorkexps[]>(rootURL + '/EmployeeWorkExps/' + EmpId).pipe(
      catchError(MemberService.handleError)
    );
  }

  getWorkExperienceById(WorkID: number): Observable<EmployeeFamilyinfo[]> {
    return this.httpClient.get<EmployeeFamilyinfo[]>(rootURL + '/GettblEmployeeWorkExpByWorkID/' + WorkID).pipe(
      catchError(MemberService.handleError)
    );
  }
  insertEmpWorkExperience(data: EmployeeWorkexps): Observable<EmployeeWorkexps> {
    return this.httpClient.post<EmployeeWorkexps>(rootURL + '/EmployeeWorkExps', data);
  }
  updateEmpWorkExperience(data: EmployeeWorkexps): Observable<EmployeeWorkexps> {
    return this.httpClient.put<EmployeeWorkexps>(rootURL + '/EmployeeWorkExps/' + data.WorkID, data);
  }
  deleteWorkExperience(Id: number) {
    return this.httpClient.delete(rootURL + '/EmployeeWorkExps/' + Id);
  }

  getEmpLeaveApplicationById(EmpId: number): Observable<Leaveapplication[]> {
    return this.httpClient.get<Leaveapplication[]>(rootURL + '/LeaveApplications/' + EmpId).pipe(
      catchError(MemberService.handleError)
    );
  }

  getPerticularLeaveById(Id: number): Observable<Leaveapplication[]> {
    return this.httpClient.get<Leaveapplication[]>(rootURL + '/GettblLeaveApplicationById/' + Id).pipe(
      catchError(MemberService.handleError)
    );
  }

  insertApplyLeave(data: Leaveapplication): Observable<Leaveapplication> {
    return this.httpClient.post<Leaveapplication>(rootURL + '/LeaveApplications', data);
  }

  updateEmpLeave(data: Leaveapplication): Observable<Leaveapplication> {
    return this.httpClient.put<Leaveapplication>(rootURL + '/LeaveApplications/' + data.Id, data);
  }

  deleteAppliedLeave(Id: number) {
    return this.httpClient.delete(rootURL + '/LeaveApplications/' + Id);
  }

}
