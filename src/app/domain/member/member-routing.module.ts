import { EditLeaveApplicationComponent } from './component/leave-application/edit-leave-application/edit-leave-application.component';
import { LeaveApplicationListComponent } from './component/leave-application/leave-application-list/leave-application-list.component';
import { AddDepartmentComponent } from './component/department/add-department/add-department.component';
import { AddPositionComponent } from './component/position/add-position/add-position.component';
import { DepartmentListComponent } from './component/department/department-list/department-list.component';
import { PositionListComponent } from './component/position/position-list/position-list.component';
import { AddRoleComponent } from './component/Role/add-role/add-role.component';
import { RoleListComponent } from './component/Role/role-list/role-list.component';
import { AddCompanyComponent } from './component/company/add-company/add-company.component';
import { CompanyListComponent } from './component/company/company-list/company-list.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticationGuard } from '../../core/guards/authentication.guard';
import { AddEmployeeComponent } from './component/employee/add-employee/add-employee.component';
import { EmployeeListComponent } from './component/employee/employee-list/employee-list.component';
import { AddHolidayComponent } from './component/holiday-calander/add-holiday/add-holiday.component';
import { HolidayListComponent } from './component/holiday-calander/holiday-list/holiday-list.component';
import { AddHolidayTypeComponent } from './component/holiday-type/add-holiday-type/add-holiday-type.component';
import { HolidayTypeListComponent } from './component/holiday-type/holiday-type-list/holiday-type-list.component';
import { MonthlySalaryComponent } from './component/monthly-salary/monthly-salary.component';
import { AddEmployeeSalaryComponent } from './component/salary/add-employee-salary/add-employee-salary.component';
import { EmployeeSalaryComponent } from './component/salary/employee-salary/employee-salary.component';
import { MainLayoutComponent } from './layout/main-layout/main-layout.component';

const routes: Routes = [
  {
    path: 'account',
    children: [
      {
        path: '',
        // loadChildren: () => import('./components/member-create-account/member-create-account.module')
        //   .then(m => m.MemberCreateAccountModule)
      }
    ]
  },
  {
    path: '',
    component: MainLayoutComponent,
    data: { canOpenSideNav: true },
    canActivate: [ AuthenticationGuard ],
    children: [
      { path: '', component: EmployeeListComponent },
      { path: 'add-employee', component: AddEmployeeComponent },
      { path: 'add-employee/:id', component: AddEmployeeComponent },
      { path: 'employee-salary', component: EmployeeSalaryComponent },
      { path: 'add-salary-details', component: AddEmployeeSalaryComponent },
      { path: 'add-salary-details/:id', component: AddEmployeeSalaryComponent },
      { path: 'monthly-salary', component: MonthlySalaryComponent },
      { path: 'holiday-list', component: HolidayListComponent },
      { path: 'add-holiday', component: AddHolidayComponent },
      { path: 'add-holiday/:id', component: AddHolidayComponent },
      { path: 'holiday-type-list', component: HolidayTypeListComponent },
      { path: 'add-holiday-type', component: AddHolidayTypeComponent },
      { path: 'add-holiday-type/:id', component: AddHolidayTypeComponent },
      { path: 'company-list', component: CompanyListComponent },
      { path: 'add-company', component: AddCompanyComponent },
      { path: 'add-company/:id', component: AddCompanyComponent },
      { path: 'role-list', component: RoleListComponent },
      { path: 'add-role', component: AddRoleComponent },
      { path: 'add-role/:id', component: AddRoleComponent },
      { path: 'position-list', component: PositionListComponent },
      { path: 'add-position', component: AddPositionComponent },
      { path: 'add-position/:id', component: AddPositionComponent },
      { path: 'department-list', component: DepartmentListComponent },
      { path: 'add-department', component: AddDepartmentComponent},
      { path: 'add-department/:id', component: AddDepartmentComponent},
      { path: 'leave-application-list', component: LeaveApplicationListComponent},
      { path: 'edit-leave-application/:id', component: EditLeaveApplicationComponent}
    ]
  },
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class MemberRoutingModule {}
