import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Company } from './../../../../../core/models/company';
import { DialogService } from './../../../../../dialog.service';
import { MemberService } from './../../../services/member.service';

@Component({
  selector: 'app-company-list',
  templateUrl: './company-list.component.html',
  styleUrls: ['./company-list.component.scss']
})
export class CompanyListComponent implements OnInit {
companyList: Company[] = [];
horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
dataSource: any;
noDataMessage: boolean = false;
displayedColumns = [ 'companyName', 'webSite', 'email', 'contactPerson', 'contactNo', 'action'];
@ViewChild(MatPaginator, {static: true}) paginator !: MatPaginator;
@ViewChild(MatSort, {static: true}) sort !: MatSort;
constructor(private memberService: MemberService,
            private dialogService: DialogService,
            private snackBar: MatSnackBar,
            private router: Router) { }

  ngOnInit(): void {
    this.getCompanyList();
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.filteredData.length === 0){
      this.noDataMessage = true;
    }
    else{
      this.noDataMessage = false;
    }
  }

  getCompanyList(): void{
    this.memberService.getCompanytList().subscribe((res: any) => {
          this.companyList = res;
          this.dataSource = new MatTableDataSource(this.companyList);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        },
        err => {
          console.log(err);
        },
      );
}

onClickAdd(): void{
  this.router.navigate([ '/member/add-company' ]).then();
}

deleteCompany(value: any): void{

  this.dialogService.openConfirmDialog('Are you sure you want to delete this record ?')
  .afterClosed().subscribe (res => {
    if (res){
      this.memberService.deleteCompany(value.CompanyID).subscribe(res => {
        this.snackBar.open('Delete Successfully...!', undefined, {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition
        });
        this.getCompanyList();
      });
    }
  });

}

editCompany(val: any): void{
  this.router.navigate([ '/member/add-company/' + val.CompanyID ]).then();
}

}
