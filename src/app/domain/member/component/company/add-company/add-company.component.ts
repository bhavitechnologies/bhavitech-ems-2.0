import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { Router, ActivatedRoute } from '@angular/router';
import { City } from './../../../../../core/models/city';
import { Company } from './../../../../../core/models/company';
import { Country } from './../../../../../core/models/country';
import { State } from './../../../../../core/models/state';
import { MemberService } from './../../../services/member.service';

@Component({
  selector: 'app-add-company',
  templateUrl: './add-company.component.html',
  styleUrls: ['./add-company.component.scss']
})
export class AddCompanyComponent implements OnInit {
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  myEmployeeList: Company[] = [];
  myCityList: City[] = [];
  myCountryList: Country[] = [];
  myStateList: State[] = [];
  insert: boolean = true;
  update: boolean = false;
  tempId: any;
  getByIdDataOfCompany: any;
  form: FormGroup;
  constructor(private memberService: MemberService,
              private router: Router,
              private route: ActivatedRoute,
              private snackBar: MatSnackBar,
              private formBuilder: FormBuilder) {
                this.form = this.formBuilder.group({
                  CompanyName: [ '', Validators.required ],
                  Address: [ '', Validators.required ],
                  CountryName: [ '', Validators.required ],
                  StateName: [ '', Validators.required ],
                  CityName: [ '', Validators.required ],
                  PostalCode: [ '', Validators.required ],
                  Website: [ '', Validators.required ],
                  Email: [ '', Validators.required ],
                  ContactPerson: [ '', Validators.required ],
                  ContactNo: [ '', Validators.required ],
                  FaxNo: [ '', Validators.required ],
                  PanNo: [ '', Validators.required ],
                  GSTNo: [ '', Validators.required ],
                  CINNo: [ '', Validators.required ],
                  CountryID: [ '', Validators.required ],
                  StateID: [ '', Validators.required ],
                  CityID: [ '', Validators.required ]
                });
              }

  ngOnInit(): void {
    this.tempId = this.route.snapshot.params['id'];
    if (this.tempId){
    this.getCompanyByID();
    }
    this.getCityList();
    this.getCountryList();
    this.getStateList();
  }

  getCompanyByID(): void{
  this.memberService.getCompanyByID(this.tempId).subscribe((res: any) => {
    this.getByIdDataOfCompany = res;
    this.form.patchValue({
      CompanyName: this.getByIdDataOfCompany.CompanyName,
      Address: this.getByIdDataOfCompany.Address,
      CountryID: this.getByIdDataOfCompany.CountryID,
      StateID: this.getByIdDataOfCompany.StateID,
      CityID: this.getByIdDataOfCompany.CityID,
      PostalCode: this.getByIdDataOfCompany.PostalCode,
      Website: this.getByIdDataOfCompany.Website,
      Email: this.getByIdDataOfCompany.Email,
      ContactPerson: this.getByIdDataOfCompany.ContactPerson,
      ContactNo: this.getByIdDataOfCompany.ContactNo,
      FaxNo: this.getByIdDataOfCompany.FaxNo,
      PanNo: this.getByIdDataOfCompany.PanNo,
      GSTNo: this.getByIdDataOfCompany.GSTNo,
      CINNo: this.getByIdDataOfCompany.CINNo
    });
    if (this.getByIdDataOfCompany.CityID){
      this.update = true;
      this.insert = false;
    }
  },
  err => {
    console.log(err);
  },
);
  }

getCityList(): void{
  this.memberService.getCityList().subscribe((res: any) => {
          this.myCityList = res;
        },
        err => {
          console.log(err);
        },
      );
}

getCountryList(): void{
  this.memberService.getCountryList().subscribe((res: any) => {
        this.myCountryList = res;
      },
      err => {
        console.log(err);
      },
    );
}

getStateList(): void{
  this.memberService.getStateList().subscribe((res: any) => {
        this.myStateList = res;
      },
      err => {
        console.log(err);
      },
    );
}

insertCompany(): void{
  this.memberService.insertCompany(this.form.value).subscribe((res: any) => {
    if (res === 1) {
      this.snackBar.open('Something went wrong....!', undefined, {
        duration: 2000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition
      });
    }
    else {
      this.snackBar.open('Company Added Successfully...!', undefined, {
        duration: 2000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition
      });
      this.router.navigate([ '/member/company-list' ]).then();
    }
  },
  err => {
    console.log(err);
  },
);
}

onClickCancel(): void{
  this.router.navigate([ '/member/company-list' ]).then();
}

updateCompany(): void{
  this.form.value.CompanyID = this.tempId;
  this.memberService.updateCompany(this.form.value).subscribe((res: any) => {
    this.snackBar.open('Company Updated Successfully...!', undefined, {
        duration: 2000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition
      });
    this.router.navigate([ '/member/company-list' ]).then();
  },
  err => {
    console.log(err);
  },
);
}
}
