import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { delay } from 'rxjs/operators';
import { EmployeeForm } from './../../../../../core/models/employee-form.model';
import { DialogService } from './../../../../../dialog.service';
import { MemberService } from './../../../services/member.service';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.scss']
})
export class EmployeeListComponent implements OnInit {
  myEmployeeList: EmployeeForm[] = [];
  dataSource: any;
  noDataMessage: boolean = false;
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  displayedColumns = [ 'employeeCode', 'name', 'email', 'contactNo', 'department', 'action' ];
  @ViewChild(MatPaginator, {static: true}) paginator !: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort !: MatSort;
  constructor(private memberService: MemberService,
              private router: Router,
              private dialogService: DialogService,
              private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.getEmployeeList();
  }

  getEmployeeList(): void{
    this.memberService.getList().subscribe((res: any) => {
          this.myEmployeeList = res;
          this.dataSource = new MatTableDataSource(this.myEmployeeList);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        },
        err => {
          console.log(err);
        },
      );
}

applyFilter(event: Event): void {
  const filterValue = (event.target as HTMLInputElement).value;
  this.dataSource.filter = filterValue.trim().toLowerCase();
  if (this.dataSource.filteredData.length === 0){
    this.noDataMessage = true;
  }
  else{
    this.noDataMessage = false;
  }
}

addEmployee(): void{
  this.router.navigate([ '/member/add-employee' ]).then();
}

onEdit(val: any): void{
  this.router.navigate([ '/member/add-employee/' + val.Id ]).then();
}

onDelete(value: any): void{
  this.dialogService.openConfirmDialog('Are you sure you want to delete this record ?')
  .afterClosed().subscribe (res => {
    if (res){
      this.memberService.deleteEmployee(value.Id).subscribe(res => {
        this.snackBar.open('Delete Successfully...!', undefined, {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition
        });
        this.getEmployeeList();
      });
    }
  });
}

}
