import { EmployeeForm } from './../../../../../core/models/employee-form.model';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { Router, ActivatedRoute } from '@angular/router';
// import { MainLayoutService } from '../../layout/main-layout/main-layout.service';
import { MainLayoutService } from '../../../layout/main-layout/main-layout.service';
import { Company } from './../../../../../core/models/company';
import { Department } from './../../../../../core/models/department';
import { Position } from './../../../../../core/models/position';
import { Role } from './../../../../../core/models/role';
import { MemberService } from './../../../services/member.service';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.scss']
})
export class AddEmployeeComponent implements OnInit {
horizontalPosition: MatSnackBarHorizontalPosition = 'end';
verticalPosition: MatSnackBarVerticalPosition = 'top';
form: FormGroup;
insert: boolean = true;
update: boolean = false;
tempId: any;
getByIdDataOfEmp: EmployeeForm[] = [];
myCompanyList: Company[] = [];
myRoleList: Role[] = [];
myDepartmentList: Department[] = [];
myPositionList: Position[] = [];
  constructor(private router: Router,
              private route: ActivatedRoute,
              private memberService: MemberService,
              private formBuilder: FormBuilder,
              private snackBar: MatSnackBar,
              public mainLayoutService: MainLayoutService) {
                this.form = this.formBuilder.group({
                  FirstName: [ '', Validators.required ],
                  MiddleName: [ '', Validators.required ],
                  LastName: [ '', Validators.required ],
                  email: [ '', Validators.required ],
                  password: [ '', Validators.required ],
                  CompanyID: [ '', Validators.required ],
                  RoleID: [ '', Validators.required ],
                  DOB: [ '', Validators.required ],
                  ContactNo: [ '', Validators.required ],
                  employeeCode: [ '', Validators.required ],
                  gender: [ '', Validators.required ],
                  DepartmentID: [ '', Validators.required ],
                  PositionID: [ '', Validators.required ],
                  dateOfJoining: [ '', Validators.required ],
                  terminatedate: [ '', Validators.required ],
                  CountryID: [ null ],
                  StateID: [ null ],
                  CityID: [ null],
                });
               }

  ngOnInit(): void {
    this.tempId = this.route.snapshot.params['id'];
    if (this.tempId){
      this.getByIdDataOfEmployee();
    }
    this.getComapanyList();
    this.getRoleList();
    this.getDepartmentList();
    this.getPositionList();
  }

  getByIdDataOfEmployee(): void{
    this.memberService.getEmpDetailsByID(this.tempId).subscribe((res: any) => {
      this.getByIdDataOfEmp = res;
      this.form.patchValue({
        FirstName: this.getByIdDataOfEmp[0].FirstName,
        MiddleName: this.getByIdDataOfEmp[0].MiddleName,
        LastName: this.getByIdDataOfEmp[0].LastName,
        email: this.getByIdDataOfEmp[0].Email,
        password: this.getByIdDataOfEmp[0].Password,
        CompanyID: this.getByIdDataOfEmp[0].CompanyID,
        RoleID: this.getByIdDataOfEmp[0].RoleID,
        DOB: this.getByIdDataOfEmp[0].DOB,
        ContactNo: this.getByIdDataOfEmp[0].ContactNo,
        employeeCode: this.getByIdDataOfEmp[0].EmployeeCode,
        gender: this.getByIdDataOfEmp[0].Gender,
        DepartmentID: this.getByIdDataOfEmp[0].DepartmentID,
        PositionID: this.getByIdDataOfEmp[0].PositionID,
        dateOfJoining: this.getByIdDataOfEmp[0].DateOfJoining,
        terminatedate: this.getByIdDataOfEmp[0].TerminateDate
      });
      if (this.getByIdDataOfEmp[0].Id){
        this.update = true;
        this.insert = false;
      }
    },
    err => {
      console.log(err);
    },
  );
  }

  getComapanyList(): void{
    this.memberService.getCompanytList().subscribe((res: any) => {
          this.myCompanyList = res;
        },
        err => {
          console.log(err);
        },
      );
}

getRoleList(): void{
  this.memberService.getRoleList().subscribe((res: any) => {
        this.myRoleList = res;
      },
      err => {
        console.log(err);
      },
    );
}

getDepartmentList(): void{
  this.memberService.getDepartmentList().subscribe((res: any) => {
        this.myDepartmentList = res;
      },
      err => {
        console.log(err);
      },
    );
}

getPositionList(): void{
  this.memberService.getPositionList().subscribe((res: any) => {
        this.myPositionList = res;
      },
      err => {
        console.log(err);
      },
    );
}

insertEmployee(): void{
  this.form.value.CountryID = 1;
  this.form.value.StateID = 1;
  this.form.value.CityID = 1;
  this.memberService.insertEmployee(this.form.value).subscribe((res: any) => {
    if (res === 1) {
      this.snackBar.open('Email is already exist,Please enter different email....!', undefined, {
        duration: 2000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition
      });
    }
    else {
      this.snackBar.open('Employee Added Successfully...!', undefined, {
        duration: 2000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition
      });
      this.router.navigate([ '/member' ]).then();
    }
  },
  err => {
    console.log(err);
  },
);
}

  cancel(): void {
    this.router.navigate([ '/member' ]).then();
  }

  updateEmployee(): void{
    this.form.value.Id = this.tempId;
    this.memberService.updateEmployee(this.form.value).subscribe((res: any) => {
      if (res === 1) {
        this.snackBar.open('Employee Updated Successfully...!', undefined, {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition
        });
        this.router.navigate([ '/member' ]).then();
      }
      else {
        this.snackBar.open('Something went wrong....!', undefined, {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition
        });
      }
    },
    err => {
      console.log(err);
    },
  );

  }
}
