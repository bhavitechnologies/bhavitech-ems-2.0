import { Holidaycalender } from './../../../../../core/models/holidaycalender.model';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { Router, ActivatedRoute } from '@angular/router';
import { Holidaytype } from './../../../../../core/models/holidaytype.model';
import { MainLayoutService } from './../../../layout/main-layout/main-layout.service';
import { MemberService } from './../../../services/member.service';

@Component({
  selector: 'app-add-holiday',
  templateUrl: './add-holiday.component.html',
  styleUrls: ['./add-holiday.component.scss']
})
export class AddHolidayComponent implements OnInit {
  form: FormGroup;
  tempId: any;
  insert: boolean = true;
  selectedHolidaySequence = false;
  selectedSequence: any;
  update: boolean = false;
  holidayListDataById: any;
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  myHolidayTypeList: Holidaytype[] = [];
  constructor(private router: Router,
              private route: ActivatedRoute,
              private memberService: MemberService,
              private formBuilder: FormBuilder,
              private snackBar: MatSnackBar,
              public mainLayoutService: MainLayoutService) {
                 this.form = this.formBuilder.group({
                  HolidayName: [ '', Validators.required ],
                  HolidayID: [ '', Validators.required ],
                  Year: [ '', Validators.required ],
                  Frequency: [ '', Validators.required ],
                  FromDate: [ '', Validators.required ],
                  ToDate: [ '' ],
                 });
               }

  ngOnInit(): void {
    this.tempId = this.route.snapshot.params['id'];
    if (this.tempId){
      this.getHolidayListDataById();
    }
    this.getHolidaysTypeList();
  }

  getHolidayListDataById(): void{
    this.memberService.getHolidayListByID(this.tempId).subscribe((res: any) => {
      this.holidayListDataById = res;
      this.form.patchValue({
        HolidayName: this.holidayListDataById.HolidayName,
        HolidayID: this.holidayListDataById.HolidayID,
        Year: this.holidayListDataById.Year,
        Frequency: this.holidayListDataById.Frequency,
        FromDate: this.holidayListDataById.FromDate,
        ToDate: this.holidayListDataById.ToDate,
      });
      if (this.holidayListDataById.Id){
        this.update = true;
        this.insert = false;
      }
      if (this.holidayListDataById.Frequency === 'Multiple'){
        this.selectedHolidaySequence = true;
        }
    },
    err => {
      console.log(err);
    },
  );
  }
  getHolidaysTypeList(): void{
    this.memberService.getHolidaysTypeList().subscribe((res: any) => {
          this.myHolidayTypeList = res;
        },
        err => {
          console.log(err);
        },
      );
}

insertHoliday(): void{
  this.memberService.insertHoliday(this.form.value).subscribe((res: any) => {
    if (res) {
      this.snackBar.open('Holiday Added Successfully...!', undefined, {
        duration: 2000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition
      });
      this.router.navigate([ '/member/holiday-list' ]).then();
    }
    else {
      this.snackBar.open('Something went wrong....!', undefined, {
        duration: 2000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition
      });
    }
  },
  err => {
    console.log(err);
  },
);
}

onClickCancel(): void{
  this.router.navigate([ '/member/holiday-list' ]).then();
}

updateHoliday(): void{
  this.form.value.Id = this.tempId;
  this.memberService.updateHolidayList(this.form.value).subscribe((res: any) => {
    this.snackBar.open('Holiday Calander updated Successfully...!', undefined, {
        duration: 2000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition
      });
    this.router.navigate([ '/member/holiday-list' ]).then();
  },
  err => {
    console.log(err);
  },
);
}

onChange(value: any): void {

  this.selectedSequence = value;
  if (this.selectedSequence === 'Multiple'){
    this.selectedHolidaySequence = true;
  }
  else{
    this.selectedHolidaySequence = false;
    this.form.value.ToDate = '';
  }
}

}
