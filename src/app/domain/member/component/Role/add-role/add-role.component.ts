import { MainLayoutService } from './../../../layout/main-layout/main-layout.service';
import { MemberService } from './../../../services/member.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-role',
  templateUrl: './add-role.component.html',
  styleUrls: ['./add-role.component.scss']
})
export class AddRoleComponent implements OnInit {
  form: FormGroup;
  insert: boolean = true;
  update: boolean = false;
  tempId: any;
  getRoleById: any;
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
verticalPosition: MatSnackBarVerticalPosition = 'top';
  constructor(private router: Router,
              private route: ActivatedRoute,
              private memberService: MemberService,
              private formBuilder: FormBuilder,
              private snackBar: MatSnackBar,
              public mainLayoutService: MainLayoutService) {
    this.form = this.formBuilder.group({
      RoleID: [ '', Validators.required ],
      Role: [ '', Validators.required ],
    });
   }

  ngOnInit(): void {
    this.tempId = this.route.snapshot.params['id'];
    if (this.tempId){
      this.getByIdRole();
    }
  }

  getByIdRole(): void{
    this.memberService.getRoleByID(this.tempId).subscribe((res: any) => {
      this.getRoleById = res;
      this.form.patchValue({
        RoleID: this.getRoleById.RoleID.toString(),
        Role: this.getRoleById.Role
      });
      if (this.getRoleById.Id){
        this.update = true;
        this.insert = false;
      }
    },
    err => {
      console.log(err);
    },
  );
  }

  insertRole(): void{

    this.memberService.insertRole(this.form.value).subscribe((res: any) => {
      if (res === 1) {
        this.snackBar.open('Something went wrong....!', undefined, {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition
        });
      }
      else {
        this.snackBar.open('Role Added Successfully...!', undefined, {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition
        });
        this.router.navigate([ '/member/role-list' ]).then();
      }
    },
    err => {
      console.log(err);
    },
  );
  }

  onClickCancel(): void{
    this.router.navigate([ '/member/role-list' ]).then();
  }

  updateRole(): void{

    this.form.value.Id = this.tempId;
    this.memberService.updateRole(this.form.value).subscribe((res: any) => {

      if (res === 0) {
        this.snackBar.open('Role updated Successfully...!', undefined, {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition
        });
        this.router.navigate([ '/member/role-list' ]).then();
      }
      else {
        this.snackBar.open('Something went wrong....!', undefined, {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition
        });
      }
    },
    err => {
      console.log(err);
    },
  );
  }
}
