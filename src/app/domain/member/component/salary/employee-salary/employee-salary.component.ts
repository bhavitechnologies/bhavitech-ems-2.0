import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Salary } from './../../../../../core/models/salary';
import { DialogService } from './../../../../../dialog.service';
import { MemberService } from './../../../services/member.service';

@Component({
  selector: 'app-employee-salary',
  templateUrl: './employee-salary.component.html',
  styleUrls: ['./employee-salary.component.scss']
})
export class EmployeeSalaryComponent implements OnInit {
  myEmployeeSalaryList: Salary[] = [];
  dataSource: any;
  noDataMessage: boolean = false;
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  displayedColumns = [ 'employeeCode', 'employeeName', 'action'];
  @ViewChild(MatPaginator, {static: true}) paginator !: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort !: MatSort;
  constructor(private memberService: MemberService,
              private dialogService: DialogService,
              private snackBar: MatSnackBar,
              private router: Router) { }

  ngOnInit(): void {
    this.getEmployeeSalaryList();
  }

  getEmployeeSalaryList(): void{
    this.memberService.getSalaryList().subscribe((res: any) => {
          this.myEmployeeSalaryList = res;
          this.dataSource = new MatTableDataSource(this.myEmployeeSalaryList);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        },
        err => {
          console.log(err);
        },
      );
}

addNewEmployeeSalaryDetails(): void{
  this.router.navigate([ '/member/add-salary-details' ]).then();
}

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.filteredData.length === 0){
      this.noDataMessage = true;
    }
    else{
      this.noDataMessage = false;
    }
  }

  onDelete(value: any): void{
    this.dialogService.openConfirmDialog('Are you sure you want to delete this record ?')
    .afterClosed().subscribe (res => {
      if (res){
        this.memberService.deleteSalaryDetails(value.Id).subscribe(res => {
          this.snackBar.open('Delete Successfully...!', undefined, {
            duration: 2000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition
          });
          this.getEmployeeSalaryList();
        });
      }
    });
  }

  onEdit(val: any): void{
    this.router.navigate([ '/member/add-salary-details/' + val.Id ]).then();
  }

}
