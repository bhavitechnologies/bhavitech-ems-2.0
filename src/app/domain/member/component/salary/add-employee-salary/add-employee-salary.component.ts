import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { Router, ActivatedRoute } from '@angular/router';
import { EmployeeForm } from './../../../../../core/models/employee-form.model';
import { MemberService } from './../../../services/member.service';

@Component({
  selector: 'app-add-employee-salary',
  templateUrl: './add-employee-salary.component.html',
  styleUrls: ['./add-employee-salary.component.scss']
})
export class AddEmployeeSalaryComponent implements OnInit {
horizontalPosition: MatSnackBarHorizontalPosition = 'end';
verticalPosition: MatSnackBarVerticalPosition = 'top';
tempId: any;
insert: boolean = true;
update: boolean = false;
getByIdSalaryOfEmp: any;
myEmployeeList: EmployeeForm[] = [];
form: FormGroup;
  constructor(private memberService: MemberService,
              private router: Router,
              private route: ActivatedRoute,
              private snackBar: MatSnackBar,
              private formBuilder: FormBuilder) {
                this.form = this.formBuilder.group({
                  EmpId: [ '', Validators.required ],
                  BankName: [ '', Validators.required ],
                  AccountNo: [ '', Validators.required ],
                  ReEnterAccountNo: [ '', Validators.required ],
                  AccountHolderName: [ '', Validators.required ],
                  IFSCcode: [ '', Validators.required ],
                  BasicSalary: [ 0, Validators.required ],
                  PT: [ 0, Validators.required ],
                  HRA: [ 0, Validators.required ],
                  TDS: [ 0, Validators.required ],
                  SA: [ 0, Validators.required ],
                  OtherDed: [ 0, Validators.required ],
                  TA: [ 0, Validators.required ],
                  Leave: [ 0, Validators.required ],
                  MA: [ 0, Validators.required ],
                  TaxDeduction: [ 0, Validators.required ],
                  CCA: [ 0, Validators.required ],
                  NetPay: [ 0, Validators.required ],
                  LTC: [ 0, Validators.required ],
                  MR: [ 0, Validators.required ],
                  GrossPay: [ 0, Validators.required ],
                });
               }

  ngOnInit(): void {
    this.tempId = this.route.snapshot.params['id'];
    if (this.tempId){
      this.getByIdSalaryOfEmployee();
    }
    this.getEmployeeName();
  }

  getByIdSalaryOfEmployee(): void{
    this.memberService.getEmpSalaryDetailsByID(this.tempId).subscribe((res: any) => {
      this.getByIdSalaryOfEmp = res;
      this.form.patchValue({
        EmpId: this.getByIdSalaryOfEmp.EmpId,
        BankName: this.getByIdSalaryOfEmp.BankName,
        AccountNo: this.getByIdSalaryOfEmp.AccountNo,
        ReEnterAccountNo: this.getByIdSalaryOfEmp.ReEnterAccountNo,
        AccountHolderName: this.getByIdSalaryOfEmp.AccountHolderName,
        IFSCcode: this.getByIdSalaryOfEmp.IFSCcode,
        BasicSalary: this.getByIdSalaryOfEmp.BasicSalary,
        PT: this.getByIdSalaryOfEmp.PT,
        HRA: this.getByIdSalaryOfEmp.HRA,
        TDS: this.getByIdSalaryOfEmp.TDS,
        SA: this.getByIdSalaryOfEmp.SA,
        OtherDed: this.getByIdSalaryOfEmp.OtherDed,
        TA: this.getByIdSalaryOfEmp.TA,
        Leave: this.getByIdSalaryOfEmp.Leave,
        MA: this.getByIdSalaryOfEmp.MA,
        TaxDeduction: this.getByIdSalaryOfEmp.TaxDeduction,
        CCA: this.getByIdSalaryOfEmp.CCA,
        NetPay: this.getByIdSalaryOfEmp.NetPay,
        LTC: this.getByIdSalaryOfEmp.LTC,
        MR: this.getByIdSalaryOfEmp.MR,
        GrossPay: this.getByIdSalaryOfEmp.GrossPay
      });

      if (this.getByIdSalaryOfEmp.Id){
        this.update = true;
        this.insert = false;
      }
    },
    err => {
      console.log(err);
    },
  );
  }

  getEmployeeName(): void{
    this.memberService.getEmployeeName().subscribe((res: any) => {
      this.myEmployeeList = res;
    },
    err => {
      console.log(err);
    },
  );
  }

  onClickCancel(): void{
    this.router.navigate([ '/member/employee-salary' ]).then();
  }

  insertEmployee(): void{
    this.memberService.insertSalaryDetails(this.form.value).subscribe((res: any) => {
        this.snackBar.open('Insert Salary Details Successfully...!', undefined, {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition
        });
        this.router.navigate([ '/member/employee-salary' ]).then();
    },
    err => {
      console.log(err);
    },
  );
  }

  updateEmployee(): void{
    this.form.value.Id = this.tempId;

    this.memberService.updateEmployeeSalary(this.form.value).subscribe((res: any) => {
      this.snackBar.open('Employee Salary Updated Successfully...!', undefined, {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition
        });
      this.router.navigate([ '/member/employee-salary' ]).then();
    },
    err => {
      console.log(err);
    },
  );
  }

}
