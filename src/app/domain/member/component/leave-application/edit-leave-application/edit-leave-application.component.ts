import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { MainLayoutService } from './../../../layout/main-layout/main-layout.service';
import { MemberService } from './../../../services/member.service';

@Component({
  selector: 'app-edit-leave-application',
  templateUrl: './edit-leave-application.component.html',
  styleUrls: ['./edit-leave-application.component.scss']
})
export class EditLeaveApplicationComponent implements OnInit {
  form: FormGroup;
  tempId: any;
  selectedStatusforReject = false;
  selectedStatus = '';
  perticularLeaveData: any;
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
verticalPosition: MatSnackBarVerticalPosition = 'top';
  constructor(private router: Router,
              private route: ActivatedRoute,
              private memberService: MemberService,
              private formBuilder: FormBuilder,
              private snackBar: MatSnackBar,
              public mainLayoutService: MainLayoutService) {
                this.form = this.formBuilder.group({
                  Status: [ '', Validators.required ],
                  Comment: [ '', Validators.required ]
                });
               }

  ngOnInit(): void {
    this.tempId = this.route.snapshot.params['id'];
    if (this.tempId){
      this.getPerticularLeaveData();
    }
  }

  getPerticularLeaveData(): void{
    this.memberService.getPerticularLeaveData(this.tempId).subscribe((res: any) => {
      this.perticularLeaveData = res;
      this.form.patchValue({
        Status: this.perticularLeaveData[0].Status,
        Comment: this.perticularLeaveData[0].Comment,
      });
      if (this.perticularLeaveData[0].Status === 'Reject'){
      this.selectedStatusforReject = true;
      }
    },
    err => {
      console.log(err);
    },
  );
  }

  onChange(value: any): void {
    this.selectedStatus = value;
    if (this.selectedStatus === 'Reject'){
      this.selectedStatusforReject = true;
    }
    else{
      this.selectedStatusforReject = false;
    }
  }

  onClickCancel(): void{
    this.router.navigate([ '/member/leave-application-list' ]).then();
  }

  updateLeaveApplication(): void{

    this.form.value.Id = this.tempId;
    this.memberService.updatePerticularLeaveData(this.form.value).subscribe((res: any) => {
      if (res === 1){

        this.snackBar.open('Leave Application updated Successfully...!', undefined, {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition
        });
        this.router.navigate([ '/member/leave-application-list' ]).then();
      }
      else{
        this.snackBar.open('Something went wrong...!', undefined, {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition
        });
      }
    },
    err => {
      console.log(err);
    },
  );

  }

}
