import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { EmployeeForm } from './../../../../../core/models/employee-form.model';
import { Leaveapplication } from './../../../../../core/models/leave-application';
import { DialogService } from './../../../../../dialog.service';
import { MemberService } from './../../../services/member.service';

@Component({
  selector: 'app-leave-application-list',
  templateUrl: './leave-application-list.component.html',
  styleUrls: ['./leave-application-list.component.scss']
})
export class LeaveApplicationListComponent implements OnInit {
  myLeaveApplicationList: Leaveapplication[] = [];
  dataSource: any;
  noDataMessage: boolean = false;
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  displayedColumns = [ 'name', 'leaveType', 'fromDate', 'toDate', 'reasonForLeave', 'status', 'action' ];
  @ViewChild(MatPaginator, {static: true}) paginator !: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort !: MatSort;
  constructor(private memberService: MemberService,
              private router: Router,
              private dialogService: DialogService,
              private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.getLeaveApplicationList();
  }

  getLeaveApplicationList(): void{
    this.memberService.getLeaveApplicationList().subscribe((res: any) => {
          this.myLeaveApplicationList = res;
          this.dataSource = new MatTableDataSource(this.myLeaveApplicationList);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        },
        err => {
          console.log(err);
        },
      );
}

applyFilter(event: Event): void {
  const filterValue = (event.target as HTMLInputElement).value;
  this.dataSource.filter = filterValue.trim().toLowerCase();

  if (this.dataSource.filteredData.length === 0){
    this.noDataMessage = true;
  }
  else{
    this.noDataMessage = false;
  }
}

editLeaveApplication(val: any): void{
  this.router.navigate([ '/member/edit-leave-application/' + val.Id ]).then();
}

deleteLeaveApplication(value: any): void{

  this.dialogService.openConfirmDialog('Are you sure you want to delete this record ?')
  .afterClosed().subscribe (res => {
    if (res){
      this.memberService.deleteLeaveApplication(value.Id).subscribe(res => {
        this.snackBar.open('Delete Successfully...!', undefined, {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition
        });
        this.getLeaveApplicationList();
      });
    }
  });
}

}
