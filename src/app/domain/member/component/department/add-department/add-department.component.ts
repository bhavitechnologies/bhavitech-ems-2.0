import { MainLayoutService } from './../../../layout/main-layout/main-layout.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MemberService } from './../../../services/member.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';

@Component({
  selector: 'app-add-department',
  templateUrl: './add-department.component.html',
  styleUrls: ['./add-department.component.scss']
})
export class AddDepartmentComponent implements OnInit {
  form: FormGroup;
  insert: boolean = true;
  update: boolean = false;
  tempId: any;
  getDepartmentById: any;
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
verticalPosition: MatSnackBarVerticalPosition = 'top';
  constructor(private router: Router,
              private route: ActivatedRoute,
              private memberService: MemberService,
              private formBuilder: FormBuilder,
              private snackBar: MatSnackBar,
              public mainLayoutService: MainLayoutService) {
                this.form = this.formBuilder.group({
                  DepartmentName: [ '', Validators.required ]
                });
               }

  ngOnInit(): void {
    this.tempId = this.route.snapshot.params['id'];
    if (this.tempId){
      this.DepartmentById();
    }
  }

  DepartmentById(): void{
    this.memberService.getDepartmentByID(this.tempId).subscribe((res: any) => {
      this.getDepartmentById = res;
      this.form.patchValue({
        DepartmentName: this.getDepartmentById.DepartmentName
      });
      if (this.getDepartmentById.DepartmentID){
        this.update = true;
        this.insert = false;
      }
    },
    err => {
      console.log(err);
    },
  );
  }
  insertDepartment(): void{
    this.memberService.insertDepartment(this.form.value).subscribe((res: any) => {
      if (res === 1) {
        this.snackBar.open('Something went wrong....!', undefined, {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition
        });
      }
      else {
        this.snackBar.open('Department Added Successfully...!', undefined, {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition
        });
        this.router.navigate([ '/member/department-list' ]).then();
      }
    },
    err => {
      console.log(err);
    },
  );
  }

  onClickCancel(): void{
    this.router.navigate([ '/member/department-list' ]).then();
  }


  updateDepartment(): void{
    this.form.value.DepartmentID = this.tempId;

    this.memberService.updateDepartment(this.form.value).subscribe((res: any) => {
      this.snackBar.open('Department updated Successfully...!', undefined, {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition
        });
      this.router.navigate([ '/member/department-list' ]).then();
    },
    err => {
      console.log(err);
    },
  );
  }
}
