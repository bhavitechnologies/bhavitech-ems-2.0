import { Department } from './../../../../../core/models/department';
import { DialogService } from './../../../../../dialog.service';
import { Router } from '@angular/router';
import { MemberService } from './../../../services/member.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-department-list',
  templateUrl: './department-list.component.html',
  styleUrls: ['./department-list.component.scss']
})
export class DepartmentListComponent implements OnInit {
  myDepartmentList: Department[] = [];
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  dataSource: any;
  noDataMessage: boolean = false;
  displayedColumns = ['department', 'action' ];
  @ViewChild(MatPaginator, {static: true}) paginator !: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort !: MatSort;
  constructor(private memberService: MemberService,
              private router: Router,
              private dialogService: DialogService,
              private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.getPositionList();
  }
  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.filteredData.length === 0){
      this.noDataMessage = true;
    }
    else{
      this.noDataMessage = false;
    }
  }

  getPositionList(): void{
    this.memberService.getDepartmentList().subscribe((res: any) => {
          this.myDepartmentList = res;
          this.dataSource = new MatTableDataSource(this.myDepartmentList);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        },
        err => {
          console.log(err);
        },
      );
}

  addDepartment(): void{
  this.router.navigate([ '/member/add-department' ]).then();
}

  deleteDepartment(value: any): void{
    this.dialogService.openConfirmDialog('Are you sure you want to delete this record ?')
    .afterClosed().subscribe (res => {
      if (res){
        this.memberService.deleteDepartment(value.DepartmentID).subscribe(res => {
          this.snackBar.open('Delete Successfully...!', undefined, {
            duration: 2000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition
          });
          this.getPositionList();
        });
      }
    });
  }
  editDepartment(val: any): void{
    this.router.navigate([ '/member/add-department/' + val.DepartmentID ]).then();
  }
}
