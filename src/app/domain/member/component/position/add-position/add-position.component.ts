import { MainLayoutService } from './../../../layout/main-layout/main-layout.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MemberService } from './../../../services/member.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';

@Component({
  selector: 'app-add-position',
  templateUrl: './add-position.component.html',
  styleUrls: ['./add-position.component.scss']
})
export class AddPositionComponent implements OnInit {
  form: FormGroup;
  insert: boolean = true;
  update: boolean = false;
  tempId: any;
  getPositionById: any;
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
verticalPosition: MatSnackBarVerticalPosition = 'top';
  constructor(private router: Router,
              private route: ActivatedRoute,
              private memberService: MemberService,
              private formBuilder: FormBuilder,
              private snackBar: MatSnackBar,
              public mainLayoutService: MainLayoutService) {
                this.form = this.formBuilder.group({
                  PositionName: [ '', Validators.required ]
                });
              }

  ngOnInit(): void {
    this.tempId = this.route.snapshot.params['id'];
    if (this.tempId){
      this.PositionById();
    }
  }

  PositionById(): void{
    this.memberService.getPositionByID(this.tempId).subscribe((res: any) => {
      this.getPositionById = res;
      this.form.patchValue({
        PositionName: this.getPositionById.PositionName
      });
      if (this.getPositionById.PositionID){
        this.update = true;
        this.insert = false;
      }
    },
    err => {
      console.log(err);
    },
  );
  }

  insertPostion(): void{
    this.memberService.insertPosition(this.form.value).subscribe((res: any) => {
      if (res === 1) {
        this.snackBar.open('Something went wrong....!', undefined, {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition
        });
      }
      else {
        this.snackBar.open('Position Added Successfully...!', undefined, {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition
        });
        this.router.navigate([ '/member/position-list' ]).then();
      }
    },
    err => {
      console.log(err);
    },
  );
  }

  onClickCancel(): void{
    this.router.navigate([ '/member/position-list' ]).then();
  }

  updatePostion(): void{
    this.form.value.PositionID = this.tempId;

    this.memberService.updatePosition(this.form.value).subscribe((res: any) => {
      this.snackBar.open('Position updated Successfully...!', undefined, {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition
        });
      this.router.navigate([ '/member/position-list' ]).then();
    },
    err => {
      console.log(err);
    },
  );
  }
}
