import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { MainLayoutService } from './../../../layout/main-layout/main-layout.service';
import { MemberService } from './../../../services/member.service';

@Component({
  selector: 'app-add-holiday-type',
  templateUrl: './add-holiday-type.component.html',
  styleUrls: ['./add-holiday-type.component.scss']
})
export class AddHolidayTypeComponent implements OnInit {
  form: FormGroup;
  tempId: any;
  holidayTypeDataById: any;
  insert: boolean = true;
  update: boolean = false;
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
verticalPosition: MatSnackBarVerticalPosition = 'top';
  constructor(private router: Router,
              private route: ActivatedRoute,
              private memberService: MemberService,
              private formBuilder: FormBuilder,
              private snackBar: MatSnackBar,
              public mainLayoutService: MainLayoutService) {
                this.form = this.formBuilder.group({
                  HolidayID: [ '', Validators.required ],
                  HolidayType: [ '', Validators.required ]
                });
              }

  ngOnInit(): void {
    this.tempId = this.route.snapshot.params['id'];
    if (this.tempId){
      this.getHolidayTypeById();
    }
  }

  getHolidayTypeById(): void{
    this.memberService.getHolidayTypeByID(this.tempId).subscribe((res: any) => {
      this.holidayTypeDataById = res;
      this.form.patchValue({
        HolidayID: this.holidayTypeDataById.HolidayID,
        HolidayType: this.holidayTypeDataById.HolidayType,
      });
      if (this.holidayTypeDataById.Id){
        this.update = true;
        this.insert = false;
      }
    },
    err => {
      console.log(err);
    },
  );
  }
  insertHolidayType(): void{
    this.memberService.insertHolidayType(this.form.value).subscribe((res: any) => {
      if (res === 1) {
        this.snackBar.open('Something went wrong....!', undefined, {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition
        });
      }
      else {
        this.snackBar.open('Holiday Type Added Successfully...!', undefined, {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition
        });
        this.router.navigate([ '/member/holiday-type-list' ]).then();
      }
    },
    err => {
      console.log(err);
    },
  );
  }

  onClickCancel(): void{
    this.router.navigate([ '/member/holiday-type-list' ]).then();
  }

  updateHolidayType(): void{
    this.form.value.Id = this.tempId;
    this.memberService.updateHolidayType(this.form.value).subscribe((res: any) => {
      if (res === 0){

        this.snackBar.open('Holiday Type updated Successfully...!', undefined, {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition
        });
        this.router.navigate([ '/member/holiday-type-list' ]).then();
      }
      else{
        this.snackBar.open('Something went wrong...!', undefined, {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition
        });
      }
    },
    err => {
      console.log(err);
    },
  );
  }

}
