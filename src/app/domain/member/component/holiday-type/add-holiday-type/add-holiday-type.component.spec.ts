import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddHolidayTypeComponent } from './add-holiday-type.component';

describe('AddHolidayTypeComponent', () => {
  let component: AddHolidayTypeComponent;
  let fixture: ComponentFixture<AddHolidayTypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddHolidayTypeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddHolidayTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
