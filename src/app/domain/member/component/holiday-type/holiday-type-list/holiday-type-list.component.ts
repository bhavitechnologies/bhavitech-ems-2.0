import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Holidaytype } from './../../../../../core/models/holidaytype.model';
import { DialogService } from './../../../../../dialog.service';
import { MemberService } from './../../../services/member.service';

@Component({
  selector: 'app-holiday-type-list',
  templateUrl: './holiday-type-list.component.html',
  styleUrls: ['./holiday-type-list.component.scss']
})
export class HolidayTypeListComponent implements OnInit {
  myHolidayTypeList: Holidaytype[] = [];
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  dataSource: any;
  noDataMessage: boolean = false;
  displayedColumns = ['holidayType', 'frequency', 'action' ];
  @ViewChild(MatPaginator, {static: true}) paginator !: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort !: MatSort;
  constructor(private memberService: MemberService,
              private router: Router,
              private dialogService: DialogService,
              private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.getHolidaysTypeList();
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.filteredData.length === 0){
      this.noDataMessage = true;
    }
    else{
      this.noDataMessage = false;
    }
  }

  getHolidaysTypeList(): void{
    this.memberService.getHolidaysTypeList().subscribe((res: any) => {
          this.myHolidayTypeList = res;
          this.dataSource = new MatTableDataSource(this.myHolidayTypeList);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        },
        err => {
          console.log(err);
        },
      );
}

addHolidayType(): void{
  this.router.navigate([ '/member/add-holiday-type' ]).then();
}

deleteHolidayType(value: any): void{
  this.dialogService.openConfirmDialog('Are you sure you want to delete this record ?')
  .afterClosed().subscribe (res => {
    if (res){
      this.memberService.deleteHolidayType(value.Id).subscribe(res => {
        this.snackBar.open('Delete Successfully...!', undefined, {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition
        });
        this.getHolidaysTypeList();
      });
    }
  });
}

updateHolidayType(val: any): void{
  this.router.navigate([ '/member/add-holiday-type/' + val.Id ]).then();
}
}
