import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HolidayTypeListComponent } from './holiday-type-list.component';

describe('HolidayTypeListComponent', () => {
  let component: HolidayTypeListComponent;
  let fixture: ComponentFixture<HolidayTypeListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HolidayTypeListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HolidayTypeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
