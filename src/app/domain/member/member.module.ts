import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import {MatExpansionModule} from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import {MatRadioModule} from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { AddEmployeeComponent } from './component/employee/add-employee/add-employee.component';
import { EmployeeListComponent } from './component/employee/employee-list/employee-list.component';
import { AddEmployeeSalaryComponent } from './component/salary/add-employee-salary/add-employee-salary.component';
import { EmployeeSalaryComponent } from './component/salary/employee-salary/employee-salary.component';
import { FooterComponent } from './layout/footer/footer.component';
import { MainLayoutComponent } from './layout/main-layout/main-layout.component';
import { MainNavComponent } from './layout/main-nav/main-nav.component';
import { ToolbarModule } from './layout/toolbar/toolbar.module';
import { MemberRoutingModule } from './member-routing.module';
import { MonthlySalaryComponent } from './component/monthly-salary/monthly-salary.component';
import { HolidayListComponent } from './component/holiday-calander/holiday-list/holiday-list.component';
import { AddHolidayComponent } from './component/holiday-calander/add-holiday/add-holiday.component';
import { HolidayTypeListComponent } from './component/holiday-type/holiday-type-list/holiday-type-list.component';
import { AddHolidayTypeComponent } from './component/holiday-type/add-holiday-type/add-holiday-type.component';
import { CompanyListComponent } from './component/company/company-list/company-list.component';
import { AddCompanyComponent } from './component/company/add-company/add-company.component';
import { RoleListComponent } from './component/Role/role-list/role-list.component';
import { AddRoleComponent } from './component/Role/add-role/add-role.component';
import { PositionListComponent } from './component/position/position-list/position-list.component';
import { DepartmentListComponent } from './component/department/department-list/department-list.component';
import { AddPositionComponent } from './component/position/add-position/add-position.component';
import { AddDepartmentComponent } from './component/department/add-department/add-department.component';
import { LeaveApplicationListComponent } from './component/leave-application/leave-application-list/leave-application-list.component';
import { MatMenuModule } from '@angular/material/menu';
import { EditLeaveApplicationComponent } from './component/leave-application/edit-leave-application/edit-leave-application.component';
import { CountryListComponent } from './component/country/country-list/country-list.component';
import { CountryAddComponent } from './component/country/country-add/country-add.component';

@NgModule({
  declarations: [
    FooterComponent,
    MainLayoutComponent,
    MainNavComponent,
    EmployeeListComponent,
    AddEmployeeComponent,
    EmployeeSalaryComponent,
    AddEmployeeSalaryComponent,
    MonthlySalaryComponent,
    HolidayListComponent,
    AddHolidayComponent,
    HolidayTypeListComponent,
    AddHolidayTypeComponent,
    CompanyListComponent,
    AddCompanyComponent,
    RoleListComponent,
    AddRoleComponent,
    PositionListComponent,
    DepartmentListComponent,
    AddPositionComponent,
    AddDepartmentComponent,
    LeaveApplicationListComponent,
    EditLeaveApplicationComponent,
    CountryListComponent,
    CountryAddComponent
  ],
  imports: [
    CommonModule,
    MemberRoutingModule,
    ToolbarModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatButtonModule,
    MatIconModule,
    MatGridListModule,
    MatCardModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatSelectModule,
    MatProgressBarModule,
    MatSnackBarModule,
    MatTooltipModule,
    MatCheckboxModule,
    MatBottomSheetModule,
    MatDialogModule,
    ToolbarModule,
    MatRadioModule,
    MatFormFieldModule,
    MatExpansionModule,
    MatMenuModule
  ]
})
export class MemberModule {}
