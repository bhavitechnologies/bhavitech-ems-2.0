import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { AuthenticationService } from '../../../authentication/services/authentication.service';

import { MainLayoutService } from './main-layout.service';

describe('MainLayoutService', () => {
  let service: MainLayoutService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        AuthenticationService,
        MainLayoutService
      ]
    });
    service = TestBed.inject(MainLayoutService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
