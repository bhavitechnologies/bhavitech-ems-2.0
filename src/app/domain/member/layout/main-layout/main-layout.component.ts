import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MainLayoutService } from './main-layout.service';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: [ './main-layout.component.scss' ]
})
export class MainLayoutComponent implements OnInit {
  constructor(
    public mainLayoutService: MainLayoutService,
    public router: Router,
    public activatedRoute: ActivatedRoute
  ) {
    this.handleRoutes();
  }

  ngOnInit(): void {
  }

  handleRoutes(): void {
    this.activatedRoute.data.subscribe(r => {
      if (r.canOpenSideNav === true) {
        this.mainLayoutService.enableSideNav();
      } else {
        this.mainLayoutService.disableSideNav();
      }
    });
  }
}
