import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { AuthenticationService } from '../../../authentication/services/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class MainLayoutService {
  private isSmallScreen: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);
  isSmallScreen$ = this.isSmallScreen.asObservable();
  private sidenavContentOffsetTop = new BehaviorSubject(0);
  sidenavContentOffsetTop$ = this.sidenavContentOffsetTop.asObservable();
  private sideNavIsEnabled: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  sideNavIsEnabled$ = this.sideNavIsEnabled.asObservable();
  private sidenavOpened: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  sidenavOpened$ = this.sidenavOpened.asObservable();

  constructor(
    public authenticationService: AuthenticationService,
    private breakpointObserver: BreakpointObserver
  ) {
    this.breakpointObserver.observe([
      Breakpoints.Handset
    ]).subscribe(result => {
      if (result.matches) {
        this.isSmallScreen.next(true);
        this.renderHandsetLayout();
      }
    });
    this.breakpointObserver.observe([
      Breakpoints.Tablet,
      Breakpoints.Web
    ]).subscribe(result => {
      if (result.matches) {
        this.isSmallScreen.next(false);
        this.renderTabletLayout();
        this.renderWebLayout();
      }
    });
  }

  renderHandsetLayout(): void {
    this.sidenavOpened.next(false);
  }

  renderWebLayout(): void {
    this.sidenavOpened.next(true);
  }

  renderTabletLayout(): void {
    this.sidenavOpened.next(true);
  }

  toggleSidenav(): void {
    this.sidenavOpened.next(!this.sidenavOpened.getValue());
  }

  enableSideNav(): void {
    this.sideNavIsEnabled.next(true);
  }

  disableSideNav(): void {
    this.sideNavIsEnabled.next(false);
  }

  updateSidenavContentOffsetTop(value: number): void {
    this.sidenavContentOffsetTop.next(value);
  }
}
