import { Component, Input, OnInit } from '@angular/core';
import { ThemePalette } from '@angular/material/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../../authentication/services/authentication.service';
import { MainLayoutService } from '../main-layout/main-layout.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: [ './toolbar.component.scss' ]
})
export class ToolbarComponent implements OnInit {
  @Input()
  toolbarActive = false;
  @Input()
  theme: ThemePalette = 'accent';
  localStorageData: any;
  tempName: any;
  tempLastName: any;
  isAuthenticated = this.authenticationService.isAuthenticated();

  constructor(
    public mainLayoutService: MainLayoutService,
    public authenticationService: AuthenticationService,
    public router: Router
  ) {
  }

  ngOnInit(): void {
    if (!this.toolbarActive) {
      this.mainLayoutService.sidenavContentOffsetTop$.subscribe(r => r > 128 ? this.toolbarActive = true : this.toolbarActive = false);
    }
    this.localStorageData =  JSON.parse(localStorage.getItem('tempData') as string);
    this.tempName = this.localStorageData[0].FirstName;
    this.tempLastName = this.localStorageData[0].LastName;
  }

  signOut(): void{
    this.router.navigate([ '/authentication/sign-in' ]).then();
  }
}
