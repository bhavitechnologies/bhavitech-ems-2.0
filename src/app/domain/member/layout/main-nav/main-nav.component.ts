import { Component, HostBinding, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../../../authentication/services/authentication.service';
import { MainLayoutService } from '../main-layout/main-layout.service';
import { Session } from './../../../../core/models/authentication';

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: [ './main-nav.component.scss' ]
})
export class MainNavComponent implements OnInit {
  backgroundImage = null; // 'url(/assets/images/background-01-sports.jpg)';
  @HostBinding('style.background-color')
  backgroundColor = 'white';
  hr = false;
  admin = false;
  employee = false;
  localStorageData: any;

  constructor(
    public authenticationService: AuthenticationService,
    public mainLayoutService: MainLayoutService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.localStorageData =  JSON.parse(localStorage.getItem('tempData') as string);
    if (this.localStorageData[0].RoleID === 1){
        this.admin = true;
      }
    if (this.localStorageData[0].RoleID === 2){
        this.hr = true;
      }
    if (this.localStorageData[0].RoleID === 3){
        this.employee = true;
      }
  }

  onChangeRoute(): void {
    this.mainLayoutService.isSmallScreen$.subscribe(r => {
      if (r) {
        this.mainLayoutService.toggleSidenav();
      }
    });
  }

  onScroll($event: Event): void {
    this.mainLayoutService.updateSidenavContentOffsetTop(($event.target as Element).scrollTop);
  }

  signOut(): void {
    try {
      // this.authenticationService.signOut();
      this.mainLayoutService.isSmallScreen$.subscribe(r => {
        if (r) {
          this.mainLayoutService.toggleSidenav();
        }
      });
    } finally {
      this.router.navigate([ '/authentication/sign-in' ]).then();
    }
  }
}
