import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { Router, ActivatedRoute } from '@angular/router';
import { EmployeeEducation } from './../../../../core/models/employee-education';
import { MainLayoutService } from './../../../member/layout/main-layout/main-layout.service';
import { MemberService } from './../../../member/services/member.service';

@Component({
  selector: 'app-add-employee-education',
  templateUrl: './add-employee-education.component.html',
  styleUrls: ['./add-employee-education.component.scss']
})
export class AddEmployeeEducationComponent implements OnInit {
  localStorageData: any;
  tempId: any;
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  form: FormGroup;
  insert: boolean = true;
  update: boolean = false;
  urlTempId: any;
  getByIdDataOfEmpEducation: any;
  constructor(private router: Router,
              private route: ActivatedRoute,
              private memberService: MemberService,
              private formBuilder: FormBuilder,
              private snackBar: MatSnackBar,
              public mainLayoutService: MainLayoutService) {
                this.form = this.formBuilder.group({
                  SchoolUniversity: [ '', Validators.required ],
                  Degree: [ '', Validators.required ],
                  Grade: [ '', Validators.required ],
                  PassingOfYear: [ '', Validators.required ]
                });
               }

  ngOnInit(): void {
    this.localStorageData =  JSON.parse(localStorage.getItem('tempData') as string);
    this.tempId = this.localStorageData[0].Id;
    this.urlTempId = this.route.snapshot.params['id'];
    if (this.urlTempId){
      this.getEmpEducationById();
    }
  }

  getEmpEducationById(): void{

    this.memberService.getEmpEducationBy(this.urlTempId).subscribe((res: any) => {
      this.getByIdDataOfEmpEducation = res;
      this.form.patchValue({
        SchoolUniversity: this.getByIdDataOfEmpEducation[0].SchoolUniversity,
        Degree: this.getByIdDataOfEmpEducation[0].Degree,
        Grade: this.getByIdDataOfEmpEducation[0].Grade,
        PassingOfYear: this.getByIdDataOfEmpEducation[0].PassingOfYear,
      });
      if (this.getByIdDataOfEmpEducation[0].EduID){
        this.update = true;
        this.insert = false;
      }
    },
    err => {
      console.log(err);
    },
  );

  }

  insertEmpEducation(): void{
    this.form.value.EmpID = this.tempId;
    this.memberService.insertEmpEducation(this.form.value).subscribe((res: any) => {
    this.snackBar.open('Your Education Added Successfully...!', undefined, {
            duration: 2000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition
          });
    this.router.navigate([ '/employee/education-list' ]).then();
    },
    err => {
      console.log(err);
    },
  );
  }

  onClickCancel(): void{
    this.router.navigate([ '/employee/education-list' ]).then();
  }

  updateEmpEducation(): void{

    this.form.value.EduID = this.urlTempId;
  this.memberService.updateEmpEducation(this.form.value).subscribe((res: any) => {
    if (res === 1) {
      this.snackBar.open('Education Information Updated Successfully...!', undefined, {
        duration: 2000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition
      });
    this.router.navigate([ '/employee/education-list' ]).then();
    }
    else {
      this.snackBar.open('Something went wrong....!', undefined, {
        duration: 2000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition
      });
    }

  },
  err => {
    console.log(err);
  },
);
  }

}
