import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { EmployeeEducation } from './../../../../core/models/employee-education';
import { DialogService } from './../../../../dialog.service';
import { MemberService } from './../../../member/services/member.service';

@Component({
  selector: 'app-employee-education-list',
  templateUrl: './employee-education-list.component.html',
  styleUrls: ['./employee-education-list.component.scss']
})
export class EmployeeEducationListComponent implements OnInit {
  localStorageData: any;
  tempId: any;
  myEmployeeEducationList: EmployeeEducation[] = [];
  dataSource: any;
  noDataMessage: boolean = false;
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  displayedColumns = [ 'university', 'degree', 'grade', 'passingYear', 'action' ];
  @ViewChild(MatPaginator, {static: true}) paginator !: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort !: MatSort;
  constructor(private memberService: MemberService,
              private router: Router,
              private dialogService: DialogService,
              private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.localStorageData =  JSON.parse(localStorage.getItem('tempData') as string);
    this.tempId = this.localStorageData[0].Id;
    this.getEmpEducationById();
  }

  getEmpEducationById(): void{
    this.memberService.getEmpEducationById(this.tempId).subscribe((res: any) => {
          this.myEmployeeEducationList = res;
          this.dataSource = new MatTableDataSource(this.myEmployeeEducationList);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        },
        err => {
          console.log(err);
        },
      );
}

  applyFilter(event: Event): void {
  const filterValue = (event.target as HTMLInputElement).value;
  this.dataSource.filter = filterValue.trim().toLowerCase();
  if (this.dataSource.filteredData.length === 0){
    this.noDataMessage = true;
  }
  else{
    this.noDataMessage = false;
  }
}

addEmpEducation(): void{
  this.router.navigate([ '/employee/add-education' ]).then();
}

onClickDelete(value: any): void{
  this.dialogService.openConfirmDialog('Are you sure you want to delete this record ?')
  .afterClosed().subscribe (res => {
    if (res){
      this.memberService.deleteEmpEducation(value.EduID).subscribe(res => {
        this.snackBar.open('Delete Successfully...!', undefined, {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition
        });
        this.getEmpEducationById();
      });
    }
  });
}

editEmpEducation(val: any): void{
  this.router.navigate([ '/employee/add-education/' + val.EduID ]).then();
}

}
