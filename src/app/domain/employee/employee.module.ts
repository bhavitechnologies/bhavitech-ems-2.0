import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ToolbarModule } from './../member/layout/toolbar/toolbar.module';
import { EmployeePersonalInfoListComponent } from './employee-personal-info/employee-personal-info-list/employee-personal-info-list.component';
import { EmployeeRoutingModule } from './employee-routing.module';
import { EmployeeEducationListComponent } from './employee-education/employee-education-list/employee-education-list.component';
import { AddEmployeeEducationComponent } from './employee-education/add-employee-education/add-employee-education.component';
import { EmployeeFamilyInfoListComponent } from './employee-family-info/employee-family-info-list/employee-family-info-list.component';
import { AddEmployeeFamilyInfoComponent } from './employee-family-info/add-employee-family-info/add-employee-family-info.component';
import { WorkExperienceListComponent } from './employee-work-experience/work-experience-list/work-experience-list.component';
import { AddWorkExperienceComponent } from './employee-work-experience/add-work-experience/add-work-experience.component';
import { AddEmployeePersonalInfoComponent } from './employee-personal-info/add-employee-personal-info/add-employee-personal-info.component';
import { AddLeaveApplicationComponent } from './employee-leave-application/add-leave-application/add-leave-application.component';
import { ListLeaveApplicationComponent } from './employee-leave-application/list-leave-application/list-leave-application.component';

@NgModule({
  declarations: [
    EmployeePersonalInfoListComponent,
    EmployeeEducationListComponent,
    AddEmployeeEducationComponent,
    EmployeeFamilyInfoListComponent,
    AddEmployeeFamilyInfoComponent,
    WorkExperienceListComponent,
    AddWorkExperienceComponent,
    AddEmployeePersonalInfoComponent,
    AddLeaveApplicationComponent,
    ListLeaveApplicationComponent
  ],
  imports: [
    CommonModule,
    EmployeeRoutingModule,
    ToolbarModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatButtonModule,
    MatIconModule,
    MatGridListModule,
    MatCardModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatSelectModule,
    MatProgressBarModule,
    MatSnackBarModule,
    MatTooltipModule,
    MatCheckboxModule,
    MatBottomSheetModule,
    MatDialogModule,
    MatRadioModule,
    MatFormFieldModule,
    MatExpansionModule
  ]
})
export class EmployeeModule { }
