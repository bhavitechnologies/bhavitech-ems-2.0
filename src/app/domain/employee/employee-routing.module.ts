import { AddLeaveApplicationComponent } from './employee-leave-application/add-leave-application/add-leave-application.component';
import { ListLeaveApplicationComponent } from './employee-leave-application/list-leave-application/list-leave-application.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticationGuard } from './../../core/guards/authentication.guard';
import { MainLayoutComponent } from './../member/layout/main-layout/main-layout.component';
import { AddEmployeeEducationComponent } from './employee-education/add-employee-education/add-employee-education.component';
import { EmployeeEducationListComponent } from './employee-education/employee-education-list/employee-education-list.component';
import { AddEmployeeFamilyInfoComponent } from './employee-family-info/add-employee-family-info/add-employee-family-info.component';
import { EmployeeFamilyInfoListComponent } from './employee-family-info/employee-family-info-list/employee-family-info-list.component';
import { AddEmployeePersonalInfoComponent } from './employee-personal-info/add-employee-personal-info/add-employee-personal-info.component';
import { EmployeePersonalInfoListComponent } from './employee-personal-info/employee-personal-info-list/employee-personal-info-list.component';
import { AddWorkExperienceComponent } from './employee-work-experience/add-work-experience/add-work-experience.component';
import { WorkExperienceListComponent } from './employee-work-experience/work-experience-list/work-experience-list.component';

const routes: Routes = [
  {
    path: 'account',
    children: [
      {
        path: '',
        // loadChildren: () => import('./components/member-create-account/member-create-account.module')
        //   .then(m => m.MemberCreateAccountModule)
      }
    ]
  },
  {
    path: '',
    component: MainLayoutComponent,
    data: { canOpenSideNav: true },
    canActivate: [ AuthenticationGuard ],
    children: [
      { path: '', component: EmployeePersonalInfoListComponent },
      { path: 'edit-personal-info/:id', component: AddEmployeePersonalInfoComponent},
      { path: 'education-list', component: EmployeeEducationListComponent },
      { path: 'add-education', component: AddEmployeeEducationComponent },
      { path: 'add-education/:id', component: AddEmployeeEducationComponent },
      { path: 'family-info-list', component: EmployeeFamilyInfoListComponent },
      { path: 'add-family-info', component: AddEmployeeFamilyInfoComponent },
      { path: 'add-family-info/:id', component: AddEmployeeFamilyInfoComponent },
      { path: 'work-experience-list', component: WorkExperienceListComponent },
      { path: 'add-work-experience', component: AddWorkExperienceComponent },
      { path: 'add-work-experience/:id', component: AddWorkExperienceComponent },
      { path: 'leave-application-list', component: ListLeaveApplicationComponent },
      { path: 'add-leave-application', component: AddLeaveApplicationComponent },
      { path: 'add-leave-application/:id', component: AddLeaveApplicationComponent },
    ]
  },
];

@NgModule({
  // declarations: [],
  // imports: [
  //   CommonModule
  // ]
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class EmployeeRoutingModule { }
