import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListLeaveApplicationComponent } from './list-leave-application.component';

describe('ListLeaveApplicationComponent', () => {
  let component: ListLeaveApplicationComponent;
  let fixture: ComponentFixture<ListLeaveApplicationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListLeaveApplicationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListLeaveApplicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
