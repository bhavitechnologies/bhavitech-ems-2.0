import { DialogService } from './../../../../dialog.service';
import { Router } from '@angular/router';
import { MemberService } from './../../../member/services/member.service';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-list-leave-application',
  templateUrl: './list-leave-application.component.html',
  styleUrls: ['./list-leave-application.component.scss']
})
export class ListLeaveApplicationComponent implements OnInit {
  dataSource: any;
  localStorageData: any;
  tempId: any;
  noDataMessage: boolean = false;
  leaveApplicationById: any;
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  displayedColumns = [ 'name', 'leaveType', 'fromDate', 'toDate', 'reasonForLeave', 'status', 'comment', 'action' ];
  @ViewChild(MatPaginator, {static: true}) paginator !: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort !: MatSort;
  constructor(private memberService: MemberService,
              private router: Router,
              private dialogService: DialogService,
              private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.localStorageData =  JSON.parse(localStorage.getItem('tempData') as string);
    this.tempId = this.localStorageData[0].Id;
    this.getLeaveApplicationByEmpId();
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.filteredData.length === 0){
      this.noDataMessage = true;
    }
    else{
      this.noDataMessage = false;
    }
  }

  getLeaveApplicationByEmpId(): void{
    this.memberService.getEmpLeaveApplicationById(this.tempId).subscribe((res: any) => {
      this.leaveApplicationById = res;
      this.dataSource = new MatTableDataSource(this.leaveApplicationById);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },
    err => {
      console.log(err);
    },
  );

  }

  onClickApplyLeave(): void{
    this.router.navigate([ '/employee/add-leave-application' ]).then();
  }

  onClickEdit(val: any): void{
    this.router.navigate([ '/employee/add-leave-application/' + val.Id ]).then();
  }

  onClickDelete(value: any): void{
    this.dialogService.openConfirmDialog('Are you sure you want to delete this record ?')
  .afterClosed().subscribe (res => {
    if (res){
      this.memberService.deleteAppliedLeave(value.Id).subscribe(res => {
        this.snackBar.open('Delete Successfully...!', undefined, {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition
        });
        this.getLeaveApplicationByEmpId();
      });
    }
  });
  }
}
