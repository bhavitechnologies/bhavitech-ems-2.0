import { MainLayoutService } from './../../../member/layout/main-layout/main-layout.service';
import { MemberService } from './../../../member/services/member.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-leave-application',
  templateUrl: './add-leave-application.component.html',
  styleUrls: ['./add-leave-application.component.scss']
})
export class AddLeaveApplicationComponent implements OnInit {
  localStorageData: any;
  tempId: any;
  insert: boolean = true;
  update: boolean = false;
  urlTempId: any;
  getByIdDataOfLeave: any;
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  form: FormGroup;
  constructor(private router: Router,
              private memberService: MemberService,
              private route: ActivatedRoute,
              private formBuilder: FormBuilder,
              private snackBar: MatSnackBar,
              public mainLayoutService: MainLayoutService) {
                this.form = this.formBuilder.group({
                  Leavetype: [ '', Validators.required ],
                  FromDate: [ '', Validators.required ],
                  ToDate: [ '', Validators.required ],
                  Reasonforleave: [ '', Validators.required ],
                  Status: [ '', Validators.required ],
                });
              }

  ngOnInit(): void {
    this.localStorageData =  JSON.parse(localStorage.getItem('tempData') as string);
    this.tempId = this.localStorageData[0].Id;
    this.urlTempId = this.route.snapshot.params['id'];
    if (this.urlTempId){
      this.getEmpLeaveById();
    }
  }

  getEmpLeaveById(): void{
    this.memberService.getPerticularLeaveById(this.urlTempId).subscribe((res: any) => {
      this.getByIdDataOfLeave = res;
      this.form.patchValue({
        Leavetype: this.getByIdDataOfLeave[0].Leavetype,
        Status: this.getByIdDataOfLeave[0].Status,
        FromDate: this.getByIdDataOfLeave[0].FromDate,
        ToDate: this.getByIdDataOfLeave[0].ToDate,
        Reasonforleave: this.getByIdDataOfLeave[0].Reasonforleave,
      });
      if (this.getByIdDataOfLeave[0].Id){
        this.update = true;
        this.insert = false;
      }
    },
    err => {
      console.log(err);
    },
  );
  }

  insertLeaveApplication(): void{
    this.form.value.EmpID = this.tempId;
    this.memberService.insertApplyLeave(this.form.value).subscribe((res: any) => {
    this.snackBar.open('Your Leave Application Added Successfully...!', undefined, {
            duration: 2000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition
          });
    this.router.navigate([ '/employee/leave-application-list' ]).then();
    },
    err => {
      console.log(err);
    },
  );
  }

  updateLeaveApplication(): void{

    this.form.value.Id = this.urlTempId;

    this.memberService.updateEmpLeave(this.form.value).subscribe((res: any) => {
      if (res === 1) {
        this.snackBar.open('Applied Leave Updated Successfully...!', undefined, {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition
        });
        this.router.navigate([ '/employee/leave-application-list' ]).then();
      }
      else {
        this.snackBar.open('Something went wrong....!', undefined, {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition
        });
      }

    },
    err => {
      console.log(err);
    },
  );

  }

  onClickCancel(): void{
    this.router.navigate([ '/employee/leave-application-list' ]).then();
  }

}
