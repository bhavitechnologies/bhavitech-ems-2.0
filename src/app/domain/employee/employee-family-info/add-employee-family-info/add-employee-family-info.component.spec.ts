import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEmployeeFamilyInfoComponent } from './add-employee-family-info.component';

describe('AddEmployeeFamilyInfoComponent', () => {
  let component: AddEmployeeFamilyInfoComponent;
  let fixture: ComponentFixture<AddEmployeeFamilyInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEmployeeFamilyInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEmployeeFamilyInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
