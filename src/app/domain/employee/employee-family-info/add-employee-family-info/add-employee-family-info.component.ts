import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { Router, ActivatedRoute } from '@angular/router';
import { MainLayoutService } from './../../../member/layout/main-layout/main-layout.service';
import { MemberService } from './../../../member/services/member.service';

@Component({
  selector: 'app-add-employee-family-info',
  templateUrl: './add-employee-family-info.component.html',
  styleUrls: ['./add-employee-family-info.component.scss']
})
export class AddEmployeeFamilyInfoComponent implements OnInit {
  localStorageData: any;
  tempId: any;
  insert: boolean = true;
  update: boolean = false;
  urlTempId: any;
  getByIdDataOfEmpFamily: any;
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  form: FormGroup;
  constructor(private router: Router,
              private route: ActivatedRoute,
              private memberService: MemberService,
              private formBuilder: FormBuilder,
              private snackBar: MatSnackBar,
              public mainLayoutService: MainLayoutService) {
                this.form = this.formBuilder.group({
                  Name: [ '', Validators.required ],
                  Relationship: [ '', Validators.required ],
                  DOB: [ '', Validators.required ],
                  Occupation: [ '', Validators.required ],
                });
              }

  ngOnInit(): void {
    this.localStorageData =  JSON.parse(localStorage.getItem('tempData') as string);
    this.tempId = this.localStorageData[0].Id;

    this.urlTempId = this.route.snapshot.params['id'];
    if (this.urlTempId){
      this.getEmpFamilyById();
    }
  }

  getEmpFamilyById(): void{
    this.memberService.getFamilyInfoBy(this.urlTempId).subscribe((res: any) => {
      this.getByIdDataOfEmpFamily = res;
      this.form.patchValue({
        Name: this.getByIdDataOfEmpFamily[0].Name,
        Relationship: this.getByIdDataOfEmpFamily[0].Relationship,
        DOB: this.getByIdDataOfEmpFamily[0].DOB,
        Occupation: this.getByIdDataOfEmpFamily[0].Occupation,
      });
      if (this.getByIdDataOfEmpFamily[0].DepID){
        this.update = true;
        this.insert = false;
      }
    },
    err => {
      console.log(err);
    },
  );
  }
  insertEmpFamilyInfo(): void{
    this.form.value.EmpID = this.tempId;
    this.memberService.insertEmpFamilyInfo(this.form.value).subscribe((res: any) => {
    this.snackBar.open('Your Family Details Added Successfully...!', undefined, {
            duration: 2000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition
          });
    this.router.navigate([ '/employee/family-info-list' ]).then();
    },
    err => {
      console.log(err);
    },
  );
  }

  onClickCancel(): void{
    this.router.navigate([ '/employee/family-info-list' ]).then();
  }

  updateEmpFamilyInfo(): void{
    this.form.value.DepID = this.urlTempId;

    this.memberService.updateEmpFamily(this.form.value).subscribe((res: any) => {
      if (res === 1) {
        this.snackBar.open('Family Details Updated Successfully...!', undefined, {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition
        });
        this.router.navigate([ '/employee/family-info-list' ]).then();
      }
      else {
        this.snackBar.open('Something went wrong....!', undefined, {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition
        });
      }

    },
    err => {
      console.log(err);
    },
  );
  }
}
