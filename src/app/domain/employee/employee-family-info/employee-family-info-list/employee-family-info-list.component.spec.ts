import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeFamilyInfoListComponent } from './employee-family-info-list.component';

describe('EmployeeFamilyInfoListComponent', () => {
  let component: EmployeeFamilyInfoListComponent;
  let fixture: ComponentFixture<EmployeeFamilyInfoListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmployeeFamilyInfoListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeFamilyInfoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
