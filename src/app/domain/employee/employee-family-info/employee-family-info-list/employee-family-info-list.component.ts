import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { EmployeeFamilyinfo } from './../../../../core/models/employee-familyinfo';
import { DialogService } from './../../../../dialog.service';
import { MemberService } from './../../../member/services/member.service';

@Component({
  selector: 'app-employee-family-info-list',
  templateUrl: './employee-family-info-list.component.html',
  styleUrls: ['./employee-family-info-list.component.scss']
})
export class EmployeeFamilyInfoListComponent implements OnInit {
  localStorageData: any;
  tempId: any;
  myEmpFamilyInfoList: EmployeeFamilyinfo[] = [];
  dataSource: any;
  noDataMessage: boolean = false;
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  displayedColumns = [ 'name', 'relationship', 'dob', 'occupation', 'action' ];
  @ViewChild(MatPaginator, {static: true}) paginator !: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort !: MatSort;
  constructor(private memberService: MemberService,
              private router: Router,
              private dialogService: DialogService,
              private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.localStorageData =  JSON.parse(localStorage.getItem('tempData') as string);
    this.tempId = this.localStorageData[0].Id;
    this.getEmpFamilyInfoById();
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.filteredData.length === 0){
      this.noDataMessage = true;
    }
    else{
      this.noDataMessage = false;
    }
  }

  getEmpFamilyInfoById(): void{
    this.memberService.getEmpFamilyInfoById(this.tempId).subscribe((res: any) => {
          this.myEmpFamilyInfoList = res;
          this.dataSource = new MatTableDataSource(this.myEmpFamilyInfoList);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        },
        err => {
          console.log(err);
        },
      );
}

addEmpFamilyInfo(): void{
  this.router.navigate([ '/employee/add-family-info' ]).then();
}

deleteFamilyInfo(value: any): void{
  this.dialogService.openConfirmDialog('Are you sure you want to delete this record ?')
  .afterClosed().subscribe (res => {
    if (res){
      this.memberService.deleteFamilyInfo(value.DepID).subscribe(res => {
        this.snackBar.open('Delete Successfully...!', undefined, {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition
        });
        this.getEmpFamilyInfoById();
      });
    }
  });
}

editFamilyInfo(val: any): void{
  this.router.navigate([ '/employee/add-family-info/' + val.DepID ]).then();
}

}
