import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { Router, ActivatedRoute } from '@angular/router';
import { MainLayoutService } from './../../../member/layout/main-layout/main-layout.service';
import { MemberService } from './../../../member/services/member.service';

@Component({
  selector: 'app-add-work-experience',
  templateUrl: './add-work-experience.component.html',
  styleUrls: ['./add-work-experience.component.scss']
})
export class AddWorkExperienceComponent implements OnInit {
  localStorageData: any;
  tempId: any;
  insert: boolean = true;
  update: boolean = false;
  urlTempId: any;
  getByIdDataOfEmpWork: any;
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  form: FormGroup;
  constructor(private router: Router,
              private memberService: MemberService,
              private route: ActivatedRoute,
              private formBuilder: FormBuilder,
              private snackBar: MatSnackBar,
              public mainLayoutService: MainLayoutService) {
                this.form = this.formBuilder.group({
                  CompanyName: [ '', Validators.required ],
                  Designation: [ '', Validators.required ],
                  FromDate: [ '', Validators.required ],
                  ToDate: [ '', Validators.required ],
                });
               }

  ngOnInit(): void {
    this.localStorageData =  JSON.parse(localStorage.getItem('tempData') as string);
    this.tempId = this.localStorageData[0].Id;
    this.urlTempId = this.route.snapshot.params['id'];
    if (this.urlTempId){
      this.getEmpWorkExById();
    }
  }

  getEmpWorkExById(): void{
    this.memberService.getWorkExperienceById(this.urlTempId).subscribe((res: any) => {
      this.getByIdDataOfEmpWork = res;
      this.form.patchValue({
        CompanyName: this.getByIdDataOfEmpWork[0].CompanyName,
        Designation: this.getByIdDataOfEmpWork[0].Designation,
        FromDate: this.getByIdDataOfEmpWork[0].FromDate,
        ToDate: this.getByIdDataOfEmpWork[0].ToDate,
      });
      if (this.getByIdDataOfEmpWork[0].WorkID){
        this.update = true;
        this.insert = false;
      }
    },
    err => {
      console.log(err);
    },
  );
  }
  insertEmpWorkExperience(): void{
    this.form.value.EmpID = this.tempId;
    this.memberService.insertEmpWorkExperience(this.form.value).subscribe((res: any) => {
    this.snackBar.open('Your Work Experience Added Successfully...!', undefined, {
            duration: 2000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition
          });
    this.router.navigate([ '/employee/work-experience-list' ]).then();
    },
    err => {
      console.log(err);
    },
  );
  }

  onClickCancel(): void{
    this.router.navigate([ '/employee/work-experience-list' ]).then();
  }

  updateEmpWorkExperience(): void{
    this.form.value.WorkID = this.urlTempId;

    this.memberService.updateEmpWorkExperience(this.form.value).subscribe((res: any) => {
      if (res === 1) {
        this.snackBar.open('Work Experience Updated Successfully...!', undefined, {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition
        });
        this.router.navigate([ '/employee/work-experience-list' ]).then();
      }
      else {
        this.snackBar.open('Something went wrong....!', undefined, {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition
        });
      }

    },
    err => {
      console.log(err);
    },
  );
  }

}
