import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { EmployeeWorkexps } from './../../../../core/models/employee-workexps';
import { DialogService } from './../../../../dialog.service';
import { MemberService } from './../../../member/services/member.service';

@Component({
  selector: 'app-work-experience-list',
  templateUrl: './work-experience-list.component.html',
  styleUrls: ['./work-experience-list.component.scss']
})
export class WorkExperienceListComponent implements OnInit {
  localStorageData: any;
  tempId: any;
  myEmpWorkExperienceList: EmployeeWorkexps[] = [];
  dataSource: any;
  noDataMessage: boolean = false;
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  displayedColumns = [ 'companyName', 'designation', 'fromDate', 'toDate', 'action' ];
  @ViewChild(MatPaginator, {static: true}) paginator !: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort !: MatSort;
  constructor(private memberService: MemberService,
              private router: Router,
              private dialogService: DialogService,
              private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.localStorageData =  JSON.parse(localStorage.getItem('tempData') as string);
    this.tempId = this.localStorageData[0].Id;
    this.getEmpWorkExperienceById();
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.filteredData.length === 0){
      this.noDataMessage = true;
    }
    else{
      this.noDataMessage = false;
    }
  }

  getEmpWorkExperienceById(): void{
    this.memberService.getEmpWorkExperienceById(this.tempId).subscribe((res: any) => {
          this.myEmpWorkExperienceList = res;
          this.dataSource = new MatTableDataSource(this.myEmpWorkExperienceList);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        },
        err => {
          console.log(err);
        },
      );
}

addEmpworkExperience(): void{
  this.router.navigate([ '/employee/add-work-experience' ]).then();
}

deleteWorkExperience(value: any): void{
  this.dialogService.openConfirmDialog('Are you sure you want to delete this record ?')
  .afterClosed().subscribe (res => {
    if (res){
      this.memberService.deleteWorkExperience(value.WorkID).subscribe(res => {
        this.snackBar.open('Delete Successfully...!', undefined, {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition
        });
        this.getEmpWorkExperienceById();
      });
    }
  });
}

editWorkExperience(val: any): void{
  this.router.navigate([ '/employee/add-work-experience/' + val.WorkID ]).then();
}

}
