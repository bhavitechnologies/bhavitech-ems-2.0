import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { City } from './../../../../core/models/city';
import { Company } from './../../../../core/models/company';
import { Country } from './../../../../core/models/country';
import { State } from './../../../../core/models/state';
import { MainLayoutService } from './../../../member/layout/main-layout/main-layout.service';
import { MemberService } from './../../../member/services/member.service';

@Component({
  selector: 'app-add-employee-personal-info',
  templateUrl: './add-employee-personal-info.component.html',
  styleUrls: ['./add-employee-personal-info.component.scss']
})
export class AddEmployeePersonalInfoComponent implements OnInit {
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  form: FormGroup;
  tempId: any;
  email_readonly = false;
  myEmployeeList: any;
  myCityList: City[] = [];
  myCountryList: Country[] = [];
  myStateList: State[] = [];
  constructor(private router: Router,
              private route: ActivatedRoute,
              private memberService: MemberService,
              private formBuilder: FormBuilder,
              private snackBar: MatSnackBar,
              public mainLayoutService: MainLayoutService) {
                this.form = this.formBuilder.group({
                  FirstName: [ '', Validators.required ],
                  MiddleName: [ '', Validators.required ],
                  LastName: [ '', Validators.required ],
                  Email: [ '', Validators.required ],
                  Age: [ '', Validators.required ],
                  Gender: [ '', Validators.required ],
                  ContactNo: [ '', Validators.required ],
                  EmergencyContactNo: [ '', Validators.required ],
                  PANcardNo: [ '', Validators.required ],
                  DOB: [ '', Validators.required ],
                  BloodGroup: [ '', Validators.required ],
                  Hobbies: [ '', Validators.required ],
                  Pincode: [ '', Validators.required ],
                  Address1: [ '', Validators.required ],
                  Address2: [ '', Validators.required ],
                  Address3: [ '', Validators.required ],
                  CountryID: [ '', Validators.required ],
                  StateID: [ '', Validators.required ],
                  CityID: [ '', Validators.required ],
                  IsSameAsPresent: [ '', Validators.required ],
                });
               }

  ngOnInit(): void {
    this.tempId = this.route.snapshot.params['id'];
    if (this.tempId){
    this.getEmpDetailsByID();
    }
    this.getCityList();
    this.getCountryList();
    this.getStateList();
  }

  getEmpDetailsByID(): void{

    this.memberService.getEmpDetailsByID(this.tempId).subscribe((res: any) => {
          this.myEmployeeList = res;
          this.email_readonly = true;
          this.form.patchValue({
            FirstName: this.myEmployeeList[0].FirstName,
            MiddleName: this.myEmployeeList[0].MiddleName,
            LastName: this.myEmployeeList[0].LastName,
            Email: this.myEmployeeList[0].Email,
            Age: this.myEmployeeList[0].Age,
            Gender: this.myEmployeeList[0].Gender,
            ContactNo: this.myEmployeeList[0].ContactNo,
            EmergencyContactNo: this.myEmployeeList[0].EmergencyContactNo,
            PANcardNo: this.myEmployeeList[0].PANcardNo,
            DOB: this.myEmployeeList[0].DOB,
            BloodGroup: this.myEmployeeList[0].BloodGroup,
            Hobbies: this.myEmployeeList[0].Hobbies,
            Pincode: this.myEmployeeList[0].Pincode,
            Address1: this.myEmployeeList[0].Address1,
            Address2: this.myEmployeeList[0].Address2,
            Address3: this.myEmployeeList[0].Address3,
            CountryID: this.myEmployeeList[0].CountryID,
            CityID: this.myEmployeeList[0].CityID,
            StateID: this.myEmployeeList[0].StateID,
            IsSameAsPresent: this.myEmployeeList[0].IsSameAsPresent,
          });
        },
        err => {
          console.log(err);
        },
      );
}

  getCityList(): void{
    this.memberService.getCityList().subscribe((res: any) => {
            this.myCityList = res;
          },
          err => {
            console.log(err);
          },
        );
  }

  getCountryList(): void{
    this.memberService.getCountryList().subscribe((res: any) => {
          this.myCountryList = res;
        },
        err => {
          console.log(err);
        },
      );
  }

  getStateList(): void{
    this.memberService.getStateList().subscribe((res: any) => {
          this.myStateList = res;
        },
        err => {
          console.log(err);
        },
      );
  }

  cancel(): void{
    this.router.navigate([ '/employee' ]).then();
  }

  onClickSubmit(): void{
    this.form.value.Id = this.tempId;
    this.memberService.updateEmpDetails(this.form.value).subscribe((res: any) => {
      if (res === 1) {
        this.snackBar.open('Employee Details Updated Successfully...!', undefined, {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition
        });
        this.router.navigate([ '/employee' ]).then();
      }
      else {
        this.snackBar.open('Something went wrong....!', undefined, {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition
        });
      }
      // this.snackBar.open('Employee Details Updated Successfully...!', undefined, {
      //     duration: 2000,
      //     horizontalPosition: this.horizontalPosition,
      //     verticalPosition: this.verticalPosition
      //   });
      // this.router.navigate([ '/employee' ]).then();
    },
    err => {
      console.log(err);
    },
  );
  }

}
