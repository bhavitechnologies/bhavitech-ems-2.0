import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEmployeePersonalInfoComponent } from './add-employee-personal-info.component';

describe('AddEmployeePersonalInfoComponent', () => {
  let component: AddEmployeePersonalInfoComponent;
  let fixture: ComponentFixture<AddEmployeePersonalInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEmployeePersonalInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEmployeePersonalInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
