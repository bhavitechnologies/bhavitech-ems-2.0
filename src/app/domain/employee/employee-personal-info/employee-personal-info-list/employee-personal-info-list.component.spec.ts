import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeePersonalInfoListComponent } from './employee-personal-info-list.component';

describe('EmployeePersonalInfoListComponent', () => {
  let component: EmployeePersonalInfoListComponent;
  let fixture: ComponentFixture<EmployeePersonalInfoListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmployeePersonalInfoListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeePersonalInfoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
