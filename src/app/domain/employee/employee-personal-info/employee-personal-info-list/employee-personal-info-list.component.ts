import { Component, OnInit } from '@angular/core';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { EmployeeForm } from './../../../../core/models/employee-form.model';
import { DialogService } from './../../../../dialog.service';
import { MemberService } from './../../../member/services/member.service';

@Component({
  selector: 'app-employee-personal-info-list',
  templateUrl: './employee-personal-info-list.component.html',
  styleUrls: ['./employee-personal-info-list.component.scss']
})
export class EmployeePersonalInfoListComponent implements OnInit {
  localStorageData: any;
  tempId: any;
  myEmployeeList: EmployeeForm[] = [];
  dataSource: any;
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  displayedColumns = [ 'employeeCode', 'name', 'email', 'contactNo', 'department', 'action' ];
  constructor(private memberService: MemberService,
              private router: Router,
              private dialogService: DialogService,
              private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.localStorageData =  JSON.parse(localStorage.getItem('tempData') as string);
    this.tempId = this.localStorageData[0].Id;
    this.getEmpDetailsByID();
  }

  getEmpDetailsByID(): void{
    this.memberService.getEmpDetailsByID(this.tempId).subscribe((res: any) => {
          this.myEmployeeList = res;
          this.dataSource = new MatTableDataSource(this.myEmployeeList);
        },
        err => {
          console.log(err);
        },
      );
}

editPersonalInfo(val: any): void{
  this.router.navigate([ '/employee/edit-personal-info/' + val.Id ]).then();
}
}
