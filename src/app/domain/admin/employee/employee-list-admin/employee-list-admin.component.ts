import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { EmployeeForm } from './../../../../core/models/employee-form.model';
import { DialogService } from './../../../../dialog.service';
import { MemberService } from './../../../member/services/member.service';

@Component({
  selector: 'app-employee-list-admin',
  templateUrl: './employee-list-admin.component.html',
  styleUrls: ['./employee-list-admin.component.scss']
})
export class EmployeeListAdminComponent implements OnInit {
  myEmployeeList: EmployeeForm[] = [];
  dataSource: any;
  noDataMessage: boolean = false;
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  displayedColumns = [ 'employeeCode', 'name', 'email', 'contactNo', 'department', 'action' ];
  @ViewChild(MatPaginator, {static: true}) paginator !: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort !: MatSort;
  constructor(private memberService: MemberService,
              private router: Router,
              private dialogService: DialogService,
              private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.getEmployeeList();
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.filteredData.length === 0){
      this.noDataMessage = true;
    }
    else{
      this.noDataMessage = false;
    }
  }

  getEmployeeList(): void{
    this.memberService.getList().subscribe((res: any) => {
          this.myEmployeeList = res;
          this.dataSource = new MatTableDataSource(this.myEmployeeList);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        },
        err => {
          console.log(err);
        },
      );
}
}
