import { MainLayoutComponent } from './../member/layout/main-layout/main-layout.component';
import { AuthenticationGuard } from './../../core/guards/authentication.guard';
import { EmployeeListAdminComponent } from './employee/employee-list-admin/employee-list-admin.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
    path: 'account',
    children: [
      {
        path: '',
        // loadChildren: () => import('./components/member-create-account/member-create-account.module')
        //   .then(m => m.MemberCreateAccountModule)
      }
    ]
  },
  {
    path: '',
    component: MainLayoutComponent,
    data: { canOpenSideNav: true },
    canActivate: [ AuthenticationGuard ],
    children: [
      { path: '', component: EmployeeListAdminComponent },
    ]
  },

];

@NgModule({
  // declarations: [],
  // imports: [
  //   CommonModule
  // ]
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class AdminRoutingModule { }
