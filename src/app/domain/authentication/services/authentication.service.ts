import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
// import { environment } from '../../../../environments/environment.example';
import { rootURL } from '../../../../environments/environment.example';
import { Session } from '../../../core/models/authentication';
import { MemberAccount } from '../../../core/models/member-account';
import { Login } from './../../../core/models/authentication';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  // api = `${ environment.api }`;
  apiNew = `${rootURL}`;
  options = {
    headers: new HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json'
    })
  };
  private session = new BehaviorSubject<Session | null>(null);

  constructor(private http: HttpClient) {
  }

  isAuthenticated(): boolean {
    // return !!this.session.getValue();
    return true;
  }


  login(formData: Login) {
    return this.http.post( rootURL + '/Login', formData);
  }

}
