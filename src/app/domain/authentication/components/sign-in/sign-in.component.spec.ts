import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { SignInComponent } from './sign-in.component';

describe('SignInComponent', () => {
  let component: SignInComponent;
  let fixture: ComponentFixture<SignInComponent>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignInComponent ],
      imports: [
        BrowserAnimationsModule,
        HttpClientTestingModule,
        ReactiveFormsModule,
        RouterTestingModule,
        MatButtonModule,
        MatCardModule,
        MatCheckboxModule,
        MatIconModule,
        MatInputModule
      ],
      providers: [
        FormBuilder
      ]
    })
      .compileComponents();
  }));
  beforeEach(() => {
    fixture = TestBed.createComponent(SignInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('form should be invalid', () => {
    component.form.controls.email.patchValue('');
    component.form.controls.password.patchValue('');
    fixture.detectChanges();
    expect(component.form.invalid).toBeTruthy();
    const submitEl = fixture.debugElement.query(By.css('button'));
    expect(submitEl.nativeElement.disabled).toBeTruthy();
  });
  it('form should be valid', () => {
    component.form.controls.email.patchValue('example@example.com');
    component.form.controls.password.patchValue('123456');
    fixture.detectChanges();
    expect(component.form.valid).toBeTruthy();
    const submitEl = fixture.debugElement.query(By.css('button'));
    expect(submitEl.nativeElement.disabled).toBeFalsy();
  });
});
