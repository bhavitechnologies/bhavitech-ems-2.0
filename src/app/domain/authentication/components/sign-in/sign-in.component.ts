import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { MainLayoutService } from '../../../member/layout/main-layout/main-layout.service';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: [ './sign-in.component.scss' ]
})
export class SignInComponent implements OnInit {
  form: FormGroup;
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  localStorageData: any;
  error = {
    invalidData: false,
    unexpectedError: false
  };

  constructor(
    public authenticationService: AuthenticationService,
    public mainLayoutService: MainLayoutService,
    private formBuilder: FormBuilder,
    private router: Router,
    private snackBar: MatSnackBar
  ) {
    this.form = formBuilder.group({
      email: [ '', [ Validators.email, Validators.required ] ],
      password: [ '', [ Validators.required, Validators.minLength(6), Validators.maxLength(12) ] ],
      remember_me: [ '' ]
    });
  }

  ngOnInit(): void {
  }

  onSubmit(): void {
    this.error.invalidData = false;
    this.error.unexpectedError = false;
    this.authenticationService.login(this.form.value).subscribe(
      res => {
        if (res != '' && res != null) {
          localStorage.setItem('tempData', JSON.stringify(res));
          this.mainLayoutService.enableSideNav();
          this.localStorageData =  JSON.parse(localStorage.getItem('tempData') as string);
          if (this.localStorageData[0].Role === 'Employee'){
            this.router.navigate([ '/employee' ]).then();
          }
          if (this.localStorageData[0].Role === 'Admin'){
            this.router.navigate([ '/admin' ]).then();
          }
          if (this.localStorageData[0].Role === 'HR'){
            this.router.navigate([ '/member' ]).then();
          }
          this.snackBar.open('Login Successfully....!', undefined, {
            duration: 2000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition
          });
        }else{
          this.snackBar.open('Email or Password is wrong....!', undefined, {
            duration: 2000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition
          });
        }
      },
      (error: HttpErrorResponse) => {
        if (error.status === 422) {
          this.error.invalidData = true;
        } else if (error.status === 400) {
          this.error.invalidData = true;
        } else {
          this.error.unexpectedError = true;
        }
      }
    );
  }
}
