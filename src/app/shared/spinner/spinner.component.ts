import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router } from '@angular/router';

@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: [ './spinner.component.scss' ]
})
export class SpinnerComponent implements OnInit, OnDestroy {
  public isLoading = true;

  constructor(private router: Router) {
  }

  ngOnInit(): void {
    this.router.events.subscribe(
      event => {
        if (event instanceof NavigationStart) {
          this.isLoading = true;
        } else if (event instanceof NavigationEnd ||
          event instanceof NavigationCancel ||
          event instanceof NavigationError) {
          this.isLoading = false;
        }
      },
      () => {
        this.isLoading = false;
      });
  }

  ngOnDestroy(): void {
    this.isLoading = false;
  }
}
