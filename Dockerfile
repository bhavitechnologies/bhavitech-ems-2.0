FROM nginx:1.19-alpine
COPY .docker/nginx.conf /etc/nginx/nginx.conf
COPY dist/app/ /usr/share/nginx/html
